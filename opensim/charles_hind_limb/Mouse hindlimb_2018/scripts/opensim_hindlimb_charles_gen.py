#!/usr/bin/env python

""" This file generates the opensim hindlimb model of the mouse
developed by James Charles. """

import opensim as osim
import numpy as np

SCALE = 10


class HindLimb(object):
    """Class to create the mouse hindlimb model. """

    def __init__(self, model=None, name='hindlimb'):
        """Parameters
           ----------
           self: type
               description
           model: <osim.Model>
               Opensim model instance
           name: <str>
               Name of the hindlimb model
        """
        super(HindLimb, self).__init__()
        self.model = model
        self.name = name

        if model is not None:
            self.model = model
        else:
            self.model = osim.Model()

        self.model.setName(name)
        self.body_length = 3.

        #: Add segments
        self.add_body()
        self.add_pelvis()
        self.add_femur()
        self.add_leg()
        self.add_pedal()

        #: Generate body segments
        self.generate_body_segments()

        #: Add muscles
        # self.add_muscle()

        return

    def add_body(self):
        """Add body to the simulation."""

        #: Body paramters
        _param = {}
        _param['mass'] = 0.0195
        _param['inertia'] = osim.Vec3(1.42e-006,
                                      2.54e-005,
                                      2.54e-005)

        #: Create Body
        body = osim.Body('body',
                         _param['mass'], osim.Vec3(0.0294,
                                                   -0.0041,
                                                   -0.001),
                         osim.Inertia(_param['inertia']))
        self.model.addBody(body)
        return

    def add_pelvis(self):
        """ Add pelvis segment to the simulation."""

        #: Create pelvis
        pelvis = osim.Body('pelvis',
                           0.000388, osim.Vec3(0.0029, -0.001,
                                               0.0024),
                           osim.Inertia(osim.Vec3(5.1e-009,
                                                  1.1e-008, 1.05e-008)))
        shape = osim.Mesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/charles_hind_limb/Mouse hindlimb_2018/Geometry/Pelvis_scale.stl')
        shape.set_scale_factors(osim.Vec3(SCALE))
        appearance = osim.Appearance()
        appearance.set_opacity = 0.5
        shape.set_Appearance(appearance)
        pelvis.attachGeometry(shape)
        self.model.addBody(pelvis)
        return

    def add_femur(self):
        """ Add femur segment to the simulation."""

        #: Create femur
        femur = osim.Body('femur',
                          0.0004411, osim.Vec3(-0.0028, -0.0072,
                                               0.0002),
                          osim.Inertia(osim.Vec3(2.95e-008,
                                                 7.6e-009, 3.5e-008)))
        shape = osim.Mesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/charles_hind_limb/Mouse hindlimb_2018/Geometry/Femur_scale.stl')
        shape.set_scale_factors(osim.Vec3(SCALE))
        appearance = osim.Appearance()
        appearance.set_opacity = 0.5
        shape.set_Appearance(appearance)
        femur.attachGeometry(shape)
        self.model.addBody(femur)
        return

    def add_leg(self):
        """ Add leg segment to the simulation."""

        #: Create leg
        leg = osim.Body('leg',
                        0.00021645, osim.Vec3(-0.0008, -0.0072,
                                              0.),
                        osim.Inertia(osim.Vec3(1.5e-008,
                                               8.3e-010, 1.5e-006)))
        shape = osim.Mesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/charles_hind_limb/Mouse hindlimb_2018/Geometry/TibFib_scale.stl')
        shape.set_scale_factors(osim.Vec3(SCALE))
        appearance = osim.Appearance()
        appearance.set_opacity = 0.5
        shape.set_Appearance(appearance)
        leg.attachGeometry(shape)
        self.model.addBody(leg)
        return

    def add_pedal(self):
        """ Add pedal segment to the simulation."""

        #: Create pedal
        pedal = osim.Body('pedal',
                          6.3e-5, osim.Vec3(0.0045, -0.0002,
                                            0.0001),
                          osim.Inertia(osim.Vec3(6.4e-011,
                                                 2.5e-09, 2.5e-09)))
        shape = osim.Mesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/charles_hind_limb/Mouse hindlimb_2018/Geometry/Foot2.stl')
        shape.set_scale_factors(osim.Vec3(SCALE))
        appearance = osim.Appearance()
        appearance.set_opacity = 0.5
        shape.set_Appearance(appearance)
        pedal.attachGeometry(shape)
        self.model.addBody(pedal)
        #: Add contact
        pedal_contact = osim.ContactSphere(0.001,
                                           osim.Vec3(0),
                                           pedal)
        pedal_contact.setName('pedal_contact')
        self.model.addContactGeometry(pedal_contact)
        return

    def generate_body_segments(self):
        """
        Connect the segments in the body.
        Parameters
        ----------
        """
        #: Create joints
        #: Ground-Body
        #: pylint: disable=no-member

        ground_body = osim.WeldJoint(
            'ground_body', self.model.getGround(),
            osim.Vec3(0, 0.5, 0),
            osim.Vec3(0),
            self.model.getBodySet().get('body'),
            osim.Vec3(0),
            osim.Vec3(0, 0, 0))

        #: Set coordinates
        # ground_body.set_coordinates(0,
        #                             osim.Coordinate(
        #                                 "GroundBody_Ry",
        #                                 1, 0, -3.141, 3.141))
        # ground_body.get_coordinates(0).set_clamped(False)
        # ground_body.get_coordinates(0).set_locked(True)
        # ground_body.set_coordinates(1,
        #                             osim.Coordinate(
        #                                 "Pelvic_tilt",
        #                                 1, 0.4625, -3.141, 3.141))
        # ground_body.get_coordinates(1).set_clamped(False)
        # ground_body.get_coordinates(1).set_locked(False)
        # ground_body.set_coordinates(2,
        #                             osim.Coordinate(
        #                                 "GroundBody_Rx",
        #                                 1, 0, -3.141, 3.141))
        # ground_body.get_coordinates(2).set_clamped(False)
        # ground_body.get_coordinates(2).set_locked(True)
        # ground_body.set_coordinates(3,
        #                             osim.Coordinate(
        #                                 "GroundBody_Tx",
        #                                 2, 0, -100, 100))
        # ground_body.get_coordinates(3).set_clamped(False)
        # ground_body.get_coordinates(3).set_locked(True)
        # ground_body.set_coordinates(4,
        #                             osim.Coordinate(
        #                                 "GroundBody_Ty",
        #                                 2, 0, -100, 100))
        # ground_body.get_coordinates(4).set_clamped(False)
        # ground_body.get_coordinates(4).set_locked(True)
        # ground_body.set_coordinates(5,
        #                             osim.Coordinate(
        #                                 "GroundBody_Tz",
        #                                 2, 0, -1, 1))
        # ground_body.get_coordinates(5).set_clamped(False)
        # ground_body.get_coordinates(5).set_locked(True)

        spatial_trans = osim.SpatialTransform()

        #: Set transformation axes
        trans = spatial_trans.updTransformAxis(0)
        trans.append_coordinates("PelvisBody_Ry")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(1)
        trans.append_coordinates("PelvisBody_Rz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(2)
        trans.append_coordinates("PelvisBody_Rx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(3)
        trans.append_coordinates("PelvisBody_Tx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(4)
        trans.append_coordinates("PelvisBody_Ty")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(5)
        trans.append_coordinates("PelvisBody_Tz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())

        body_pelvis = osim.CustomJoint(
            'body_pelvis', self.model.getBodySet().get('body'),
            osim.Vec3(0.1*SCALE, 0, 0),
            osim.Vec3(0),
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0),
            osim.Vec3(0, 0, 0), spatial_trans)

        #: Set coordinates
        body_pelvis.set_coordinates(0,
                                    osim.Coordinate(
                                        "PelvisBody_Ry",
                                        1, 0, -3.141, 3.141))
        body_pelvis.get_coordinates(0).set_clamped(False)
        body_pelvis.get_coordinates(0).set_locked(True)
        body_pelvis.set_coordinates(1,
                                    osim.Coordinate(
                                        "PelvisBody_Rz",
                                        1, 0.4625, -3.141, 3.141))
        body_pelvis.get_coordinates(0).set_clamped(False)
        body_pelvis.get_coordinates(0).set_locked(True)
        body_pelvis.set_coordinates(2,
                                    osim.Coordinate(
                                        "PelvisBody_Rx",
                                        1, 0, -3.141, 3.141))
        body_pelvis.get_coordinates(0).set_clamped(False)
        body_pelvis.get_coordinates(0).set_locked(True)
        body_pelvis.set_coordinates(3,
                                    osim.Coordinate(
                                        "PelvisBody_Tx",
                                        2, 0, -1, 1))
        body_pelvis.get_coordinates(3).set_clamped(False)
        body_pelvis.get_coordinates(3).set_locked(True)
        body_pelvis.set_coordinates(4,
                                    osim.Coordinate(
                                        "PelvisBody_Ty",
                                        2, 0, -1, 1))
        body_pelvis.get_coordinates(4).set_clamped(False)
        body_pelvis.get_coordinates(4).set_locked(True)
        body_pelvis.set_coordinates(5,
                                    osim.Coordinate(
                                        "PelvisBody_Tz",
                                        2, 0, -1, 1))
        body_pelvis.get_coordinates(5).set_clamped(False)
        body_pelvis.get_coordinates(5).set_locked(True)

        spatial_trans = osim.SpatialTransform()

        #: Set transformation axes
        trans = spatial_trans.updTransformAxis(0)
        trans.append_coordinates("Hip_rotation")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(1)
        trans.append_coordinates("Hip_flexion")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(2)
        trans.append_coordinates("Hip_adduction")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(3)
        trans.append_coordinates("Hip_Tx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(4)
        trans.append_coordinates("Hip_Ty")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(5)
        trans.append_coordinates("Hip_Tz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())

        hip = osim.CustomJoint(
            'hip', self.model.getBodySet().get('pelvis'),
            osim.Vec3(0, 0, 0),
            osim.Vec3(0),
            self.model.getBodySet().get('femur'),
            osim.Vec3(0),
            osim.Vec3(0, 0, 0), spatial_trans)

        #: Set coordinates
        hip.set_coordinates(0,
                            osim.Coordinate(
                                "Hip_rotation",
                                1, 0, -0.872664625997,
                                0.872664625997))
        hip.get_coordinates(0).set_clamped(False)
        hip.get_coordinates(0).set_locked(False)
        hip.set_coordinates(1,
                            osim.Coordinate(
                                "Hip_flexion",
                                1, 0, -3.141, 3.141))
        hip.get_coordinates(0).set_clamped(False)
        hip.get_coordinates(0).set_locked(False)
        hip.set_coordinates(2,
                            osim.Coordinate(
                                "Hip_adduction",
                                1, 0, -0.698131700798,
                                0.349065850399))
        hip.get_coordinates(0).set_clamped(False)
        hip.get_coordinates(0).set_locked(False)
        hip.set_coordinates(3,
                            osim.Coordinate(
                                "Hip_Tx",
                                2, 0, -1, 1))
        hip.get_coordinates(0).set_clamped(False)
        hip.get_coordinates(0).set_locked(True)
        hip.set_coordinates(4,
                            osim.Coordinate(
                                "Hip_Ty",
                                2, 0, -1, 1))
        hip.get_coordinates(0).set_clamped(False)
        hip.get_coordinates(0).set_locked(True)
        hip.set_coordinates(5,
                            osim.Coordinate(
                                "Hip_Tz",
                                2, 0, -1, 1))
        hip.get_coordinates(0).set_clamped(False)
        hip.get_coordinates(0).set_locked(True)

        spatial_trans = osim.SpatialTransform()

        #: Set transformation axes
        trans = spatial_trans.updTransformAxis(0)
        trans.append_coordinates("Knee_Ry")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(1)
        trans.append_coordinates("Knee_extension")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(2)
        trans.append_coordinates("Knee_Rx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(3)
        trans.append_coordinates("Knee_Tx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(4)
        trans.append_coordinates("Knee_Ty")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(5)
        trans.append_coordinates("Knee_Tz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())

        knee = osim.CustomJoint(
            'knee', self.model.getBodySet().get('femur'),
            osim.Vec3(0, 0, 0),
            osim.Vec3(0),
            self.model.getBodySet().get('leg'),
            osim.Vec3(0),
            osim.Vec3(0, 0, 0), spatial_trans)

        #: Set coordinates
        knee.set_coordinates(0,
                             osim.Coordinate(
                                 "Knee_Ry",
                                 1, 0, -3.141, 3.141))
        knee.get_coordinates(0).set_clamped(False)
        knee.get_coordinates(0).set_locked(True)
        knee.set_coordinates(1,
                             osim.Coordinate(
                                 "Knee_extension",
                                 1, 0, -2.530727415392,
                                 0))
        knee.get_coordinates(0).set_clamped(False)
        knee.get_coordinates(0).set_locked(False)
        knee.set_coordinates(2,
                             osim.Coordinate(
                                 "Knee_Rx",
                                 1, 0, -3.141, 3.141))
        knee.get_coordinates(0).set_clamped(False)
        knee.get_coordinates(0).set_locked(True)
        knee.set_coordinates(3,
                             osim.Coordinate(
                                 "Knee_Tx",
                                 2, -0.001281*SCALE, -1, 1))
        knee.get_coordinates(0).set_clamped(False)
        knee.get_coordinates(0).set_locked(True)
        knee.set_coordinates(4,
                             osim.Coordinate(
                                 "Knee_Ty",
                                 2, -0.01383*SCALE, -1, 1))
        knee.get_coordinates(0).set_clamped(False)
        knee.get_coordinates(0).set_locked(True)
        knee.set_coordinates(5,
                             osim.Coordinate(
                                 "Knee_Tz",
                                 2, 3.5e-005*SCALE, -1, 1))
        knee.get_coordinates(0).set_clamped(False)
        knee.get_coordinates(0).set_locked(True)

        spatial_trans = osim.SpatialTransform()

        #: Set transformation axes
        trans = spatial_trans.updTransformAxis(0)
        trans.append_coordinates("Ankle_adduction")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(1)
        trans.append_coordinates("Ankle_flexion")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(2)
        trans.append_coordinates("Ankle_inversion")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(3)
        trans.append_coordinates("Ankle_Tx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(4)
        trans.append_coordinates("Ankle_Ty")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(5)
        trans.append_coordinates("Ankle_Tz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())

        ankle = osim.CustomJoint(
            'ankle', self.model.getBodySet().get('leg'),
            osim.Vec3(0, 0, 0),
            osim.Vec3(0),
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0),
            osim.Vec3(0, 0, 0), spatial_trans)

        #: Set coordinates
        ankle.set_coordinates(0,
                              osim.Coordinate(
                                  "Ankle_adduction",
                                  1, 0, -3.141, 3.141))
        ankle.get_coordinates(0).set_clamped(False)
        ankle.get_coordinates(0).set_locked(True)
        ankle.set_coordinates(1,
                              osim.Coordinate(
                                  "Ankle_flexion",
                                  1, 0, -3.141, 3.141))
        ankle.get_coordinates(0).set_clamped(False)
        ankle.get_coordinates(0).set_locked(False)
        ankle.set_coordinates(2,
                              osim.Coordinate(
                                  "Ankle_inversion",
                                  1, 0, -3.141, 3.141))
        ankle.get_coordinates(0).set_clamped(False)
        ankle.get_coordinates(0).set_locked(True)
        ankle.set_coordinates(3,
                              osim.Coordinate(
                                  "Ankle_Tx",
                                  2, -0.000901*SCALE, -1, 1))
        ankle.get_coordinates(0).set_clamped(False)
        ankle.get_coordinates(0).set_locked(True)
        ankle.set_coordinates(4,
                              osim.Coordinate(
                                  "Ankle_Ty",
                                  2, -0.01897*SCALE, -1, 1))
        ankle.get_coordinates(0).set_clamped(False)
        ankle.get_coordinates(0).set_locked(True)
        ankle.set_coordinates(5,
                              osim.Coordinate(
                                  "Ankle_Tz",
                                  2, -0.000793*SCALE, -1, 1))
        ankle.get_coordinates(0).set_clamped(False)
        ankle.get_coordinates(0).set_locked(True)

        # #: Add joints to the model
        self.model.addJoint(ground_body)
        self.model.addJoint(body_pelvis)
        self.model.addJoint(hip)
        self.model.addJoint(knee)
        self.model.addJoint(ankle)

    def add_muscle(self):
        """Add muscle and wrapping objects"""

        # #: Add rectus femoris muscle
        # rf = osim.Millard2012EquilibriumMuscle()
        # rf.setName('rectus_femoris')
        # # Muscle attachment
        # rf.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('pelvis'),
        #     osim.Vec3(0.00215*SCALE, -0.00066*SCALE, 0.00028*SCALE))
        # rf.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('leg'),
        #     osim.Vec3(0.00146*SCALE, 0.00072*SCALE, 0.00016*SCALE))

        # # Muscle parameters
        # rf.setOptimalFiberLength(0.5555*SCALE)
        # rf.setTendonSlackLength(0.3*SCALE)
        # rf.setMaxIsometricForce(10000)
        # self.model.addForce(rf)

        # #: Add semitendanous muscle
        # sm = osim.Millard2012EquilibriumMuscle()
        # sm.setName('semitendanous')
        # # Muscle attachment
        # sm.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('pelvis'),
        #     osim.Vec3(-0.00406*SCALE, -0.00023*SCALE, 0.00133*SCALE))
        # sm.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('leg'),
        #     osim.Vec3(-9e-005*SCALE, -0.00405*SCALE, -0.00165*SCALE))

        # # Muscle parameters
        # sm.setOptimalFiberLength(0.5555*SCALE)
        # sm.setTendonSlackLength(0.3*SCALE)
        # sm.setMaxIsometricForce(10000)
        # self.model.addForce(sm)

        # #: Add gastrusmedial  muscle
        # gm_v = osim.Millard2012EquilibriumMuscle()
        # gm_v.setName('gastrusmedial_ventral')
        # # Muscle attachment
        # gm_v.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('pelvis'),
        #     osim.Vec3(0.01106*SCALE, -0.00204*SCALE, -1e-005*SCALE))
        # gm_v.addNewPathPoint(
        #     'mid-1',
        #     self.model.getBodySet().get('pelvis'),
        #     osim.Vec3(0.00731*SCALE, -0.00108*SCALE, -0.0006*SCALE))
        # gm_v.addNewPathPoint(
        #     'mid-2',
        #     self.model.getBodySet().get('femur'),
        #     osim.Vec3(-0.00064*SCALE, -4e-005*SCALE, 0.00257*SCALE))
        # gm_v.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('femur'),
        #     osim.Vec3(-0.00104*SCALE, -0.00185*SCALE, 0.00209*SCALE))

        # # Muscle parameters
        # gm_v.setOptimalFiberLength(0.5555*SCALE)
        # gm_v.setTendonSlackLength(0.3*SCALE)
        # gm_v.setMaxIsometricForce(10000)
        # self.model.addForce(gm_v)

        # # #: Add biceps_brachii_short muscle wrapping
        # # bbs_wrap = osim.WrapCylinder()
        # # bbs_wrap.setName(
        # #     'biceps_brachii_short_wrap')
        # # bbs_wrap.set_translation(
        # #     osim.Vec3(0.))
        # # bbs_wrap.set_xyz_body_rotation(
        # #     osim.Vec3(0, np.pi/2, 0))
        # # bbs_wrap.set_length(0.2*SCALE)
        # # bbs_wrap.set_radius(0.12*SCALE)
        # # bbs_wrap.set_quadrant('-y')
        # # self.model.getBodySet().get('femur').addWrapObject(
        # #     bbs_wrap)
        # # bbl.updGeometryPath().addPathWrap(bbs_wrap)
        ##########: Add GM_dorsal muscle ##########

        GM_dorsal = osim.Millard2012EquilibriumMuscle()
        GM_dorsal.setName('GM_dorsal')
        #: Muscle attachment

        GM_dorsal.addNewPathPoint(
            'GM_dorsal-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0.01186*SCALE, 0.00012*SCALE, -0.00034*SCALE))

        GM_dorsal.addNewPathPoint(
            'GM_dorsal-P2',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0.00591*SCALE, 0.00014*SCALE, -0.00066*SCALE))

        GM_dorsal.addNewPathPoint(
            'GM_dorsal-P3',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00036*SCALE, 0.00024*SCALE, 0.00247*SCALE))

        GM_dorsal.addNewPathPoint(
            'GM_dorsal-P4',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00123*SCALE, -0.00146*SCALE, 0.00211*SCALE))

        # Muscle parameters
        GM_dorsal.setOptimalFiberLength(0.01305*SCALE)
        GM_dorsal.setTendonSlackLength(0.00501*SCALE)
        GM_dorsal.setMaxIsometricForce(0.936)
        self.model.addForce(GM_dorsal)

        ##########: Add GM_mid muscle ##########

        GM_mid = osim.Millard2012EquilibriumMuscle()
        GM_mid.setName('GM_mid')
        #: Muscle attachment

        GM_mid.addNewPathPoint(
            'GM_mid-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0.01132*SCALE, -0.00086*SCALE, 0.00094*SCALE))

        GM_mid.addNewPathPoint(
            'GM_mid-P2',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0.00682*SCALE, -0.00046*SCALE, -0.00043*SCALE))

        GM_mid.addNewPathPoint(
            'GM_mid-P3',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00076*SCALE, 0.00016*SCALE, 0.00256*SCALE))

        GM_mid.addNewPathPoint(
            'GM_mid-P4',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00122*SCALE, -0.00166*SCALE, 0.0021*SCALE))

        # Muscle parameters
        GM_mid.setOptimalFiberLength(0.01271*SCALE)
        GM_mid.setTendonSlackLength(0.00489*SCALE)
        GM_mid.setMaxIsometricForce(1.026)
        self.model.addForce(GM_mid)

        ##########: Add GM_ventral muscle ##########

        GM_ventral = osim.Millard2012EquilibriumMuscle()
        GM_ventral.setName('GM_ventral')
        #: Muscle attachment

        GM_ventral.addNewPathPoint(
            'GM_ventral-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0.01106*SCALE, -0.00204*SCALE, -1e-05*SCALE))

        GM_ventral.addNewPathPoint(
            'GM_ventral-P2',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0.00731*SCALE, -0.00108*SCALE, -0.0006*SCALE))

        GM_ventral.addNewPathPoint(
            'GM_ventral-P3',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00064*SCALE, -4e-05*SCALE, 0.00257*SCALE))

        GM_ventral.addNewPathPoint(
            'GM_ventral-P4',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00104*SCALE, -0.00185*SCALE, 0.00209*SCALE))

        # Muscle parameters
        GM_ventral.setOptimalFiberLength(0.01242*SCALE)
        GM_ventral.setTendonSlackLength(0.00478*SCALE)
        GM_ventral.setMaxIsometricForce(1.049)
        self.model.addForce(GM_ventral)

        ##########: Add OE muscle ##########

        OE = osim.Millard2012EquilibriumMuscle()
        OE.setName('OE')
        #: Muscle attachment

        OE.addNewPathPoint(
            'OE-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00467*SCALE, -0.00297*SCALE, -5e-05*SCALE))

        OE.addNewPathPoint(
            'OE-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00196*SCALE, -0.00104*SCALE, 1e-05*SCALE))

        # Muscle parameters
        OE.setOptimalFiberLength(0.00246*SCALE)
        OE.setTendonSlackLength(0.00096*SCALE)
        OE.setMaxIsometricForce(0.086)
        self.model.addForce(OE)

        ##########: Add OI muscle ##########

        OI = osim.Millard2012EquilibriumMuscle()
        OI.setName('OI')
        #: Muscle attachment

        OI.addNewPathPoint(
            'OI-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00517*SCALE, -0.00434*SCALE, -0.00111*SCALE))

        OI.addNewPathPoint(
            'OI-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00085*SCALE, 2e-05*SCALE, 0.00026*SCALE))

        # Muscle parameters
        OI.setOptimalFiberLength(0.00565*SCALE)
        OI.setTendonSlackLength(0.00065*SCALE)
        OI.setMaxIsometricForce(0.314)
        self.model.addForce(OI)

        ##########: Add GEM muscle ##########

        GEM = osim.Millard2012EquilibriumMuscle()
        GEM.setName('GEM')
        #: Muscle attachment

        GEM.addNewPathPoint(
            'GEM-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00181*SCALE, 0.0011*SCALE, 0.00012*SCALE))

        GEM.addNewPathPoint(
            'GEM-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00069*SCALE, 0.00041*SCALE, 0.00069*SCALE))

        # Muscle parameters
        GEM.setOptimalFiberLength(0.00143*SCALE)
        GEM.setTendonSlackLength(1e-05*SCALE)
        GEM.setMaxIsometricForce(0.179)
        self.model.addForce(GEM)

        ##########: Add QF muscle ##########

        QF = osim.Millard2012EquilibriumMuscle()
        QF.setName('QF')
        #: Muscle attachment

        QF.addNewPathPoint(
            'QF-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00508*SCALE, -0.00446*SCALE, -0.00151*SCALE))

        QF.addNewPathPoint(
            'QF-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00189*SCALE, -0.00439*SCALE, 0.00256*SCALE))

        # Muscle parameters
        QF.setOptimalFiberLength(0.00465*SCALE)
        QF.setTendonSlackLength(0.0011*SCALE)
        QF.setMaxIsometricForce(2.03)
        self.model.addForce(QF)

        ##########: Add AM muscle ##########

        AM = osim.Millard2012EquilibriumMuscle()
        AM.setName('AM')
        #: Muscle attachment

        AM.addNewPathPoint(
            'AM-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00217*SCALE, -0.00212*SCALE, -0.00198*SCALE))

        AM.addNewPathPoint(
            'AM-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00167*SCALE, -0.0124*SCALE, 0.00176*SCALE))

        # Muscle parameters
        AM.setOptimalFiberLength(0.0076*SCALE)
        AM.setTendonSlackLength(0.00302*SCALE)
        AM.setMaxIsometricForce(0.614)
        self.model.addForce(AM)

        ##########: Add AL muscle ##########

        AL = osim.Millard2012EquilibriumMuscle()
        AL.setName('AL')
        #: Muscle attachment

        AL.addNewPathPoint(
            'AL-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00176*SCALE, -0.00212*SCALE, -0.00194*SCALE))

        AL.addNewPathPoint(
            'AL-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00149*SCALE, -0.01349*SCALE, -0.00117*SCALE))

        # Muscle parameters
        AL.setOptimalFiberLength(0.00745*SCALE)
        AL.setTendonSlackLength(0.0026*SCALE)
        AL.setMaxIsometricForce(0.402)
        self.model.addForce(AL)

        ##########: Add AB muscle ##########

        AB = osim.Millard2012EquilibriumMuscle()
        AB.setName('AB')
        #: Muscle attachment

        AB.addNewPathPoint(
            'AB-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00165*SCALE, -0.00201*SCALE, -0.00212*SCALE))

        AB.addNewPathPoint(
            'AB-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00083*SCALE, -0.01045*SCALE, -7e-05*SCALE))

        # Muscle parameters
        AB.setOptimalFiberLength(0.00642*SCALE)
        AB.setTendonSlackLength(0.0018*SCALE)
        AB.setMaxIsometricForce(0.234)
        self.model.addForce(AB)

        ##########: Add GP muscle ##########

        GP = osim.Millard2012EquilibriumMuscle()
        GP.setName('GP')
        #: Muscle attachment

        GP.addNewPathPoint(
            'GP-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00441*SCALE, -0.00311*SCALE, -0.00032*SCALE))

        GP.addNewPathPoint(
            'GP-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00011*SCALE, -0.00472*SCALE, -0.00179*SCALE))

        # Muscle parameters
        GP.setOptimalFiberLength(0.00912*SCALE)
        GP.setTendonSlackLength(0.0044*SCALE)
        GP.setMaxIsometricForce(0.345)
        self.model.addForce(GP)

        ##########: Add GA muscle ##########

        GA = osim.Millard2012EquilibriumMuscle()
        GA.setName('GA')
        #: Muscle attachment

        GA.addNewPathPoint(
            'GA-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.0044*SCALE, -0.0029*SCALE, -0.002*SCALE))

        GA.addNewPathPoint(
            'GA-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00019*SCALE, -0.0045*SCALE, -0.00173*SCALE))

        # Muscle parameters
        GA.setOptimalFiberLength(0.00882*SCALE)
        GA.setTendonSlackLength(0.00607*SCALE)
        GA.setMaxIsometricForce(0.402)
        self.model.addForce(GA)

        ##########: Add PMA muscle ##########

        PMA = osim.Millard2012EquilibriumMuscle()
        PMA.setName('PMA')
        #: Muscle attachment

        PMA.addNewPathPoint(
            'PMA-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0.01036*SCALE, 9e-05*SCALE, -0.00298*SCALE))

        PMA.addNewPathPoint(
            'PMA-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00121*SCALE, -0.00221*SCALE, 0.00028*SCALE))

        # Muscle parameters
        PMA.setOptimalFiberLength(0.00697*SCALE)
        PMA.setTendonSlackLength(0.00501*SCALE)
        PMA.setMaxIsometricForce(1.338)
        self.model.addForce(PMA)

        ##########: Add PMI muscle ##########

        PMI = osim.Millard2012EquilibriumMuscle()
        PMI.setName('PMI')
        #: Muscle attachment

        PMI.addNewPathPoint(
            'PMI-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0.008*SCALE, -0.00032*SCALE, -0.0031*SCALE))

        PMI.addNewPathPoint(
            'PMI-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00124*SCALE, -0.00204*SCALE, 0.00022*SCALE))

        # Muscle parameters
        PMI.setOptimalFiberLength(0.00578*SCALE)
        PMI.setTendonSlackLength(0.0039*SCALE)
        PMI.setMaxIsometricForce(1.088)
        self.model.addForce(PMI)

        ##########: Add ILI muscle ##########

        ILI = osim.Millard2012EquilibriumMuscle()
        ILI.setName('ILI')
        #: Muscle attachment

        ILI.addNewPathPoint(
            'ILI-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0.01019*SCALE, -0.00241*SCALE, -0.00118*SCALE))

        ILI.addNewPathPoint(
            'ILI-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00114*SCALE, -0.00234*SCALE, 0.00037*SCALE))

        # Muscle parameters
        ILI.setOptimalFiberLength(0.00857*SCALE)
        ILI.setTendonSlackLength(0.00275*SCALE)
        ILI.setMaxIsometricForce(0.549)
        self.model.addForce(ILI)

        ##########: Add PECT muscle ##########

        PECT = osim.Millard2012EquilibriumMuscle()
        PECT.setName('PECT')
        #: Muscle attachment

        PECT.addNewPathPoint(
            'PECT-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00033*SCALE, -0.0018*SCALE, -0.00167*SCALE))

        PECT.addNewPathPoint(
            'PECT-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00091*SCALE, -0.00561*SCALE, 0.00062*SCALE))

        # Muscle parameters
        PECT.setOptimalFiberLength(0.00277*SCALE)
        PECT.setTendonSlackLength(0.00181*SCALE)
        PECT.setMaxIsometricForce(0.363)
        self.model.addForce(PECT)

        ##########: Add CF muscle ##########

        CF = osim.Millard2012EquilibriumMuscle()
        CF.setName('CF')
        #: Muscle attachment

        CF.addNewPathPoint(
            'CF-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00276*SCALE, 0.00085*SCALE, 0.00085*SCALE))

        CF.addNewPathPoint(
            'CF-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00234*SCALE, -0.00792*SCALE, 0.00066*SCALE))

        CF.addNewPathPoint(
            'CF-P3',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00194*SCALE, -0.0108*SCALE, -0.00029*SCALE))

        CF.addNewPathPoint(
            'CF-P4',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00145*SCALE, -0.01321*SCALE, -0.001*SCALE))

        # Muscle parameters
        CF.setOptimalFiberLength(0.01137*SCALE)
        CF.setTendonSlackLength(0.00307*SCALE)
        CF.setMaxIsometricForce(0.554)
        self.model.addForce(CF)

        ##########: Add SM muscle ##########

        SM = osim.Millard2012EquilibriumMuscle()
        SM.setName('SM')
        #: Muscle attachment

        SM.addNewPathPoint(
            'SM-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00406*SCALE, -0.00023*SCALE, 0.00133*SCALE))

        SM.addNewPathPoint(
            'SM-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-9e-05*SCALE, -0.00405*SCALE, -0.00165*SCALE))

        # Muscle parameters
        SM.setOptimalFiberLength(0.01165*SCALE)
        SM.setTendonSlackLength(0.00409*SCALE)
        SM.setMaxIsometricForce(1.916)
        self.model.addForce(SM)

        ##########: Add ST muscle ##########

        ST = osim.Millard2012EquilibriumMuscle()
        ST.setName('ST')
        #: Muscle attachment

        ST.addNewPathPoint(
            'ST-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00569*SCALE, -0.0012*SCALE, 0.00126*SCALE))

        ST.addNewPathPoint(
            'ST-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00015*SCALE, -0.00607*SCALE, -0.00172*SCALE))

        # Muscle parameters
        ST.setOptimalFiberLength(0.01111*SCALE)
        ST.setTendonSlackLength(0.0048*SCALE)
        ST.setMaxIsometricForce(1.299)
        self.model.addForce(ST)

        ##########: Add BFA muscle ##########

        BFA = osim.Millard2012EquilibriumMuscle()
        BFA.setName('BFA')
        #: Muscle attachment

        BFA.addNewPathPoint(
            'BFA-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00312*SCALE, 0.00094*SCALE, 0.00105*SCALE))

        BFA.addNewPathPoint(
            'BFA-P2',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00182*SCALE, -0.01341*SCALE, 0.00187*SCALE))

        BFA.addNewPathPoint(
            'BFA-P3',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00193*SCALE, -0.01416*SCALE, 0.00149*SCALE))

        # Muscle parameters
        BFA.setOptimalFiberLength(0.01145*SCALE)
        BFA.setTendonSlackLength(0.00383*SCALE)
        BFA.setMaxIsometricForce(0.876)
        self.model.addForce(BFA)

        ##########: Add BFP_cranial muscle ##########

        BFP_cranial = osim.Millard2012EquilibriumMuscle()
        BFP_cranial.setName('BFP_cranial')
        #: Muscle attachment

        BFP_cranial.addNewPathPoint(
            'BFP_cranial-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00353*SCALE, 0.00068*SCALE, 0.00124*SCALE))

        BFP_cranial.addNewPathPoint(
            'BFP_cranial-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00104*SCALE, -0.00157*SCALE, 0.0012*SCALE))

        # Muscle parameters
        BFP_cranial.setOptimalFiberLength(0.01008*SCALE)
        BFP_cranial.setTendonSlackLength(0.00491*SCALE)
        BFP_cranial.setMaxIsometricForce(0.725)
        self.model.addForce(BFP_cranial)

        ##########: Add BFP_mid muscle ##########

        BFP_mid = osim.Millard2012EquilibriumMuscle()
        BFP_mid.setName('BFP_mid')
        #: Muscle attachment

        BFP_mid.addNewPathPoint(
            'BFP_mid-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00434*SCALE, 0.00021*SCALE, 0.00145*SCALE))

        BFP_mid.addNewPathPoint(
            'BFP_mid-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00187*SCALE, -0.00282*SCALE, 0.00142*SCALE))

        # Muscle parameters
        BFP_mid.setOptimalFiberLength(0.01004*SCALE)
        BFP_mid.setTendonSlackLength(0.0048*SCALE)
        BFP_mid.setMaxIsometricForce(0.728)
        self.model.addForce(BFP_mid)

        ##########: Add BFP_caudal muscle ##########

        BFP_caudal = osim.Millard2012EquilibriumMuscle()
        BFP_caudal.setName('BFP_caudal')
        #: Muscle attachment

        BFP_caudal.addNewPathPoint(
            'BFP_caudal-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(-0.00482*SCALE, -4e-05*SCALE, 0.00142*SCALE))

        BFP_caudal.addNewPathPoint(
            'BFP_caudal-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00027*SCALE, -0.0055*SCALE, 0.00133*SCALE))

        # Muscle parameters
        BFP_caudal.setOptimalFiberLength(0.01197*SCALE)
        BFP_caudal.setTendonSlackLength(0.00406*SCALE)
        BFP_caudal.setMaxIsometricForce(0.611)
        self.model.addForce(BFP_caudal)

        ##########: Add RF muscle ##########

        RF = osim.Millard2012EquilibriumMuscle()
        RF.setName('RF')
        #: Muscle attachment

        RF.addNewPathPoint(
            'RF-P1',
            self.model.getBodySet().get('pelvis'),
            osim.Vec3(0.00215*SCALE, -0.00066*SCALE, 0.00028*SCALE))

        RF.addNewPathPoint(
            'RF-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00146*SCALE, 0.00072*SCALE, 0.00016*SCALE))

        # Muscle parameters
        RF.setOptimalFiberLength(0.00534*SCALE)
        RF.setTendonSlackLength(0.00853*SCALE)
        RF.setMaxIsometricForce(4.162)
        self.model.addForce(RF)

        ##########: Add VM muscle ##########

        VM = osim.Millard2012EquilibriumMuscle()
        VM.setName('VM')
        #: Muscle attachment

        VM.addNewPathPoint(
            'VM-P1',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00029*SCALE, -0.00053*SCALE, 0.00119*SCALE))

        VM.addNewPathPoint(
            'VM-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00128*SCALE, 0.00076*SCALE, -2e-05*SCALE))

        # Muscle parameters
        VM.setOptimalFiberLength(0.00653*SCALE)
        VM.setTendonSlackLength(0.00768*SCALE)
        VM.setMaxIsometricForce(1.098)
        self.model.addForce(VM)

        ##########: Add VL muscle ##########

        VL = osim.Millard2012EquilibriumMuscle()
        VL.setName('VL')
        #: Muscle attachment

        VL.addNewPathPoint(
            'VL-P1',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00038*SCALE, -0.00062*SCALE, 0.00204*SCALE))

        VL.addNewPathPoint(
            'VL-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00128*SCALE, 0.0007*SCALE, 0.0004*SCALE))

        # Muscle parameters
        VL.setOptimalFiberLength(0.00681*SCALE)
        VL.setTendonSlackLength(0.00735*SCALE)
        VL.setMaxIsometricForce(2.828)
        self.model.addForce(VL)

        ##########: Add VI muscle ##########

        VI = osim.Millard2012EquilibriumMuscle()
        VI.setName('VI')
        #: Muscle attachment

        VI.addNewPathPoint(
            'VI-P1',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00036*SCALE, -0.00149*SCALE, 0.00146*SCALE))

        VI.addNewPathPoint(
            'VI-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00119*SCALE, 0.00073*SCALE, 0.00015*SCALE))

        # Muscle parameters
        VI.setOptimalFiberLength(0.00606*SCALE)
        VI.setTendonSlackLength(0.00702*SCALE)
        VI.setMaxIsometricForce(0.367)
        self.model.addForce(VI)

        ##########: Add POP muscle ##########

        POP = osim.Millard2012EquilibriumMuscle()
        POP.setName('POP')
        #: Muscle attachment

        POP.addNewPathPoint(
            'POP-P1',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00198*SCALE, -0.01312*SCALE, 0.00122*SCALE))

        POP.addNewPathPoint(
            'POP-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00085*SCALE, -0.00304*SCALE, -0.0011*SCALE))

        # Muscle parameters
        POP.setOptimalFiberLength(0.00206*SCALE)
        POP.setTendonSlackLength(0.00203*SCALE)
        POP.setMaxIsometricForce(0.307)
        self.model.addForce(POP)

        ##########: Add TA muscle ##########

        TA = osim.Millard2012EquilibriumMuscle()
        TA.setName('TA')
        #: Muscle attachment

        TA.addNewPathPoint(
            'TA-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00071*SCALE, -0.0025*SCALE, -5e-05*SCALE))

        TA.addNewPathPoint(
            'TA-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00157*SCALE, -0.00761*SCALE, -0.00076*SCALE))

        TA.addNewPathPoint(
            'TA-P3',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00029*SCALE, -0.01152*SCALE, -0.00042*SCALE))

        TA.addNewPathPoint(
            'TA-P4',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00041*SCALE, -0.01582*SCALE, -0.00082*SCALE))

        TA.addNewPathPoint(
            'TA-P5',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00043*SCALE, -0.01771*SCALE, -0.00098*SCALE))

        TA.addNewPathPoint(
            'TA-P6',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00084*SCALE, 1e-05*SCALE, -0.00064*SCALE))

        # Muscle parameters
        TA.setOptimalFiberLength(0.0049*SCALE)
        TA.setTendonSlackLength(0.0118*SCALE)
        TA.setMaxIsometricForce(2.422)
        self.model.addForce(TA)

        ##########: Add EDL muscle ##########

        EDL = osim.Millard2012EquilibriumMuscle()
        EDL.setName('EDL')
        #: Muscle attachment

        EDL.addNewPathPoint(
            'EDL-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00053*SCALE, -0.00295*SCALE, 0.0016*SCALE))

        EDL.addNewPathPoint(
            'EDL-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00122*SCALE, -0.01622*SCALE, 0.00047*SCALE))

        EDL.addNewPathPoint(
            'EDL-P3',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00085*SCALE, -0.01821*SCALE, 0.00026*SCALE))

        EDL.addNewPathPoint(
            'EDL-P4',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00134*SCALE, -7e-05*SCALE, 0.00027*SCALE))

        EDL.addNewPathPoint(
            'EDL-P5',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00523*SCALE, 0.00031*SCALE, 0.0*SCALE))

        EDL.addNewPathPoint(
            'EDL-P6',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00795*SCALE, 0.00091*SCALE, -6e-05*SCALE))

        EDL.addNewPathPoint(
            'EDL-P7',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00949*SCALE, 0.00109*SCALE, -0.00012*SCALE))

        EDL.addNewPathPoint(
            'EDL-P8',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01134*SCALE, 0.00045*SCALE, -0.00017*SCALE))

        EDL.addNewPathPoint(
            'EDL-P9',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01261*SCALE, -9e-05*SCALE, -0.0001*SCALE))

        EDL.addNewPathPoint(
            'EDL-P10',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01321*SCALE, -0.00114*SCALE, -0.00012*SCALE))

        EDL.addNewPathPoint(
            'EDL-P11',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01337*SCALE, -0.00172*SCALE, -0.00015*SCALE))

        EDL.addNewPathPoint(
            'EDL-P12',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01352*SCALE, -0.00193*SCALE, -0.00013*SCALE))

        # Muscle parameters
        EDL.setOptimalFiberLength(0.00635*SCALE)
        EDL.setTendonSlackLength(0.02378*SCALE)
        EDL.setMaxIsometricForce(0.368)
        self.model.addForce(EDL)

        ##########: Add EHL muscle ##########

        EHL = osim.Millard2012EquilibriumMuscle()
        EHL.setName('EHL')
        #: Muscle attachment

        EHL.addNewPathPoint(
            'EHL-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(0.00045*SCALE, -0.00418*SCALE, -0.00042*SCALE))

        EHL.addNewPathPoint(
            'EHL-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00026*SCALE, -0.01301*SCALE, -7e-05*SCALE))

        EHL.addNewPathPoint(
            'EHL-P3',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.0008*SCALE, -0.01652*SCALE, -0.00028*SCALE))

        EHL.addNewPathPoint(
            'EHL-P4',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00067*SCALE, -0.0177*SCALE, -0.00031*SCALE))

        EHL.addNewPathPoint(
            'EHL-P5',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00113*SCALE, 0.00014*SCALE, -0.00029*SCALE))

        EHL.addNewPathPoint(
            'EHL-P6',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00256*SCALE, -0.00051*SCALE, -0.00106*SCALE))

        EHL.addNewPathPoint(
            'EHL-P7',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00504*SCALE, -0.00027*SCALE, -0.00139*SCALE))

        EHL.addNewPathPoint(
            'EHL-P8',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.0073*SCALE, 0.00023*SCALE, -0.00182*SCALE))

        EHL.addNewPathPoint(
            'EHL-P9',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00867*SCALE, -0.00067*SCALE, -0.00154*SCALE))

        EHL.addNewPathPoint(
            'EHL-P10',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00928*SCALE, -0.00116*SCALE, -0.00119*SCALE))

        # Muscle parameters
        EHL.setOptimalFiberLength(0.00593*SCALE)
        EHL.setTendonSlackLength(0.01793*SCALE)
        EHL.setMaxIsometricForce(0.069)
        self.model.addForce(EHL)

        ##########: Add MG muscle ##########

        MG = osim.Millard2012EquilibriumMuscle()
        MG.setName('MG')
        #: Muscle attachment

        MG.addNewPathPoint(
            'MG-P1',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00162*SCALE, -0.01279*SCALE, -0.00078*SCALE))

        MG.addNewPathPoint(
            'MG-P2',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(-0.00205*SCALE, 3e-05*SCALE, 0.00028*SCALE))

        # Muscle parameters
        MG.setOptimalFiberLength(0.0055*SCALE)
        MG.setTendonSlackLength(0.01395*SCALE)
        MG.setMaxIsometricForce(1.75)
        self.model.addForce(MG)

        ##########: Add LG muscle ##########

        LG = osim.Millard2012EquilibriumMuscle()
        LG.setName('LG')
        #: Muscle attachment

        LG.addNewPathPoint(
            'LG-P1',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.00188*SCALE, -0.01276*SCALE, 0.00115*SCALE))

        LG.addNewPathPoint(
            'LG-P2',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(-0.002*SCALE, 0.0001*SCALE, 0.00069*SCALE))

        # Muscle parameters
        LG.setOptimalFiberLength(0.00541*SCALE)
        LG.setTendonSlackLength(0.01389*SCALE)
        LG.setMaxIsometricForce(3.784)
        self.model.addForce(LG)

        ##########: Add SOL muscle ##########

        SOL = osim.Millard2012EquilibriumMuscle()
        SOL.setName('SOL')
        #: Muscle attachment

        SOL.addNewPathPoint(
            'SOL-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00199*SCALE, -0.00813*SCALE, -0.0003*SCALE))

        SOL.addNewPathPoint(
            'SOL-P2',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(-0.00183*SCALE, 0.00021*SCALE, 0.00047*SCALE))

        # Muscle parameters
        SOL.setOptimalFiberLength(0.00316*SCALE)
        SOL.setTendonSlackLength(0.0074*SCALE)
        SOL.setMaxIsometricForce(0.591)
        self.model.addForce(SOL)

        ##########: Add PLANT muscle ##########

        PLANT = osim.Millard2012EquilibriumMuscle()
        PLANT.setName('PLANT')
        #: Muscle attachment

        PLANT.addNewPathPoint(
            'PLANT-P1',
            self.model.getBodySet().get('femur'),
            osim.Vec3(-0.0017*SCALE, -0.01247*SCALE, 0.00022*SCALE))

        PLANT.addNewPathPoint(
            'PLANT-P2',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(-0.00202*SCALE, 0.00015*SCALE, 0.00047*SCALE))

        # Muscle parameters
        PLANT.setOptimalFiberLength(0.00431*SCALE)
        PLANT.setTendonSlackLength(0.01517*SCALE)
        PLANT.setMaxIsometricForce(0.88)
        self.model.addForce(PLANT)

        ##########: Add FDL muscle ##########

        FDL = osim.Millard2012EquilibriumMuscle()
        FDL.setName('FDL')
        #: Muscle attachment

        FDL.addNewPathPoint(
            'FDL-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.0016*SCALE, -0.00195*SCALE, -0.00025*SCALE))

        FDL.addNewPathPoint(
            'FDL-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00177*SCALE, -0.01914*SCALE, -0.00115*SCALE))

        FDL.addNewPathPoint(
            'FDL-P3',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00074*SCALE, -0.00108*SCALE, -0.00031*SCALE))

        FDL.addNewPathPoint(
            'FDL-P4',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00305*SCALE, -0.00082*SCALE, -8e-05*SCALE))

        FDL.addNewPathPoint(
            'FDL-P5',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.0066*SCALE, 8e-05*SCALE, -7e-05*SCALE))

        FDL.addNewPathPoint(
            'FDL-P6',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00979*SCALE, 0.00029*SCALE, -0.00017*SCALE))

        FDL.addNewPathPoint(
            'FDL-P7',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01208*SCALE, -0.00022*SCALE, -5e-05*SCALE))

        FDL.addNewPathPoint(
            'FDL-P8',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01275*SCALE, -0.00138*SCALE, -0.00012*SCALE))

        FDL.addNewPathPoint(
            'FDL-P9',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01325*SCALE, -0.00204*SCALE, -0.00012*SCALE))

        # Muscle parameters
        FDL.setOptimalFiberLength(0.00431*SCALE)
        FDL.setTendonSlackLength(0.02761*SCALE)
        FDL.setMaxIsometricForce(1.896)
        self.model.addForce(FDL)

        ##########: Add TP muscle ##########

        TP = osim.Millard2012EquilibriumMuscle()
        TP.setName('TP')
        #: Muscle attachment

        TP.addNewPathPoint(
            'TP-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00149*SCALE, -0.00213*SCALE, -0.00069*SCALE))

        TP.addNewPathPoint(
            'TP-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00059*SCALE, -0.00349*SCALE, -0.00032*SCALE))

        TP.addNewPathPoint(
            'TP-P3',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00169*SCALE, -0.01905*SCALE, -0.00122*SCALE))

        TP.addNewPathPoint(
            'TP-P4',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00065*SCALE, -0.00088*SCALE, -0.00096*SCALE))

        # Muscle parameters
        TP.setOptimalFiberLength(0.00359*SCALE)
        TP.setTendonSlackLength(0.015*SCALE)
        TP.setMaxIsometricForce(0.549)
        self.model.addForce(TP)

        ##########: Add PL muscle ##########

        PL = osim.Millard2012EquilibriumMuscle()
        PL.setName('PL')
        #: Muscle attachment

        PL.addNewPathPoint(
            'PL-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00148*SCALE, -0.00346*SCALE, 0.00161*SCALE))

        PL.addNewPathPoint(
            'PL-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00189*SCALE, -0.00476*SCALE, 0.00097*SCALE))

        PL.addNewPathPoint(
            'PL-P3',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00125*SCALE, -0.01624*SCALE, 0.00036*SCALE))

        PL.addNewPathPoint(
            'PL-P4',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00081*SCALE, -0.00091*SCALE, 0.00094*SCALE))

        PL.addNewPathPoint(
            'PL-P5',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00134*SCALE, -0.00099*SCALE, 0.00028*SCALE))

        # Muscle parameters
        PL.setOptimalFiberLength(0.00378*SCALE)
        PL.setTendonSlackLength(0.01408*SCALE)
        PL.setMaxIsometricForce(0.647)
        self.model.addForce(PL)

        ##########: Add PT muscle ##########

        PT = osim.Millard2012EquilibriumMuscle()
        PT.setName('PT')
        #: Muscle attachment

        PT.addNewPathPoint(
            'PT-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00193*SCALE, -0.00502*SCALE, 0.00074*SCALE))

        PT.addNewPathPoint(
            'PT-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00139*SCALE, -0.01627*SCALE, 0.00021*SCALE))

        PT.addNewPathPoint(
            'PT-P3',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(-0.00011*SCALE, 0.00016*SCALE, 0.00089*SCALE))

        PT.addNewPathPoint(
            'PT-P4',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.0006*SCALE, -0.00017*SCALE, 0.00095*SCALE))

        # Muscle parameters
        PT.setOptimalFiberLength(0.00339*SCALE)
        PT.setTendonSlackLength(0.01122*SCALE)
        PT.setMaxIsometricForce(0.457)
        self.model.addForce(PT)

        ##########: Add PB muscle ##########

        PB = osim.Millard2012EquilibriumMuscle()
        PB.setName('PB')
        #: Muscle attachment

        PB.addNewPathPoint(
            'PB-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00177*SCALE, -0.00879*SCALE, -1e-05*SCALE))

        PB.addNewPathPoint(
            'PB-P2',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(-7e-05*SCALE, 0.00028*SCALE, 0.00085*SCALE))

        PB.addNewPathPoint(
            'PB-P3',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00213*SCALE, -0.00068*SCALE, 0.00101*SCALE))

        # Muscle parameters
        PB.setOptimalFiberLength(0.00229*SCALE)
        PB.setTendonSlackLength(0.01005*SCALE)
        PB.setMaxIsometricForce(0.396)
        self.model.addForce(PB)

        ##########: Add PDQA muscle ##########

        PDQA = osim.Millard2012EquilibriumMuscle()
        PDQA.setName('PDQA')
        #: Muscle attachment

        PDQA.addNewPathPoint(
            'PDQA-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00203*SCALE, -0.00541*SCALE, 0.0006*SCALE))

        PDQA.addNewPathPoint(
            'PDQA-P2',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00123*SCALE, -0.01182*SCALE, -7e-05*SCALE))

        PDQA.addNewPathPoint(
            'PDQA-P3',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00017*SCALE, 0.00023*SCALE, 0.00076*SCALE))

        PDQA.addNewPathPoint(
            'PDQA-P4',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00323*SCALE, -0.00012*SCALE, 0.0008*SCALE))

        PDQA.addNewPathPoint(
            'PDQA-P5',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00748*SCALE, 0.00058*SCALE, 0.00102*SCALE))

        PDQA.addNewPathPoint(
            'PDQA-P6',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00928*SCALE, 0.00077*SCALE, 0.00117*SCALE))

        PDQA.addNewPathPoint(
            'PDQA-P7',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01211*SCALE, 0.0006*SCALE, 0.00096*SCALE))

        PDQA.addNewPathPoint(
            'PDQA-P8',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01377*SCALE, -0.00092*SCALE, 0.00072*SCALE))

        # Muscle parameters
        PDQA.setOptimalFiberLength(0.00393*SCALE)
        PDQA.setTendonSlackLength(0.02357*SCALE)
        PDQA.setMaxIsometricForce(0.112)
        self.model.addForce(PDQA)

        ##########: Add PDQI muscle ##########

        PDQI = osim.Millard2012EquilibriumMuscle()
        PDQI.setName('PDQI')
        #: Muscle attachment

        PDQI.addNewPathPoint(
            'PDQI-P1',
            self.model.getBodySet().get('leg'),
            osim.Vec3(-0.00209*SCALE, -0.00734*SCALE, 0.00022*SCALE))

        PDQI.addNewPathPoint(
            'PDQI-P2',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00067*SCALE, -5e-05*SCALE, 0.00076*SCALE))

        PDQI.addNewPathPoint(
            'PDQI-P3',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00223*SCALE, -0.00063*SCALE, 0.00116*SCALE))

        PDQI.addNewPathPoint(
            'PDQI-P4',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.00421*SCALE, -0.00029*SCALE, 0.00107*SCALE))

        PDQI.addNewPathPoint(
            'PDQI-P5',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.0072*SCALE, -0.0002*SCALE, 0.00148*SCALE))

        PDQI.addNewPathPoint(
            'PDQI-P6',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01047*SCALE, -0.00049*SCALE, 0.00111*SCALE))

        PDQI.addNewPathPoint(
            'PDQI-P7',
            self.model.getBodySet().get('pedal'),
            osim.Vec3(0.01159*SCALE, -0.00124*SCALE, 0.00068*SCALE))

        # Muscle parameters
        PDQI.setOptimalFiberLength(0.00362*SCALE)
        PDQI.setTendonSlackLength(0.01959*SCALE)
        PDQI.setMaxIsometricForce(0.102)
        self.model.addForce(PDQI)

    def show_physical_properties(self):
        """Print physical properties of every segment in the forearm."""

        for body in self.model.getBodySet():
            _mass = body.get_mass()
            _name = body.getName()
            _inertia = []
            for j in range(0, 3):
                _inertia.append(body.get_inertia().get(j))
            print('Name : {}'.format(_name))
            print('\t Mass : {}'.format(_mass))
            print('\t Inertia : {}'.format(_inertia))

        return


def main():
    """ Main function. """

    #: osim model
    model = osim.Model()
    hindlimb = HindLimb(model)
    # hindlimb.init_arm_position()
    # hindlimb.add_muscle()
    hindlimb.show_physical_properties()

    #: Dump osim file
    file_name = './hindlimb.osim'
    model.printToXML(file_name)


if __name__ == '__main__':
    main()
