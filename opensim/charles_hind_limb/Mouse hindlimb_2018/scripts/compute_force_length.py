"""Script to compute the force-length relationship of a muscle."""
import opensim as osim
import yaml

#: Read the model
model = osim.Model('../Mouse_hindlimb_model_2018.osim')

#: Initialize the model
model.buildSystem()
state = model.initializeState()

#: Get force_sets in the model
force_set = model.getForceSet()

#: Muscle sets in the model
muscle_set = force_set.getMuscles()

#:
rf = muscle_set.get('RF')

compute_force_length(model, state, rf)


def compute_force_length(model, state, muscle):
    """ This function gets the active and passive force--length
    values across the possible fiber lengths of the muscle.
    fl_active and fl_passive are matrices containing forces
    corresponding to each fiber length. """
