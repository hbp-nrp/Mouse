"""This script generates the osim model of James Charles et al"""

import opensim as osim
import pandas as pd
import json
import yaml

#: Read the model
model = osim.Model('../Mouse_hindlimb_model_2018.osim')

#: Initialize the model
model.buildSystem()
state = model.initializeState()

#: Get force_sets in the model
force_set = model.getForceSet()

#: Muscle sets in the model
muscle_set = force_set.getMuscles()

#: Container to the muscle path point data
muscle_path_points = {}

#: Generate the list of path points for all muscles in the system
for name, muscle in muscle_set.items():
    #: container for path points of each muscle
    path_points = {}

    #: Get geometry path
    geom_path = muscle.getGeometryPath()

    #: Get path point set
    path_point_set = geom_path.getPathPointSet()

    #: Loop over all the path points
    for (path_name, point) in path_point_set.items():
        path_points[path_name] = {'body': point.getBodyName(),
                                  'point':
                                  [point.getLocation(state)[j] for j in range(0, 3)]}

    #: append muscle names
    muscle_path_points[name] = path_points

#: save the data
with open('./hind_limb_muscle_path_points.json', 'w') as outfile:
    json.dump(obj=muscle_path_points, fp=outfile, sort_keys=True,
              indent=4, separators=(',', ':'))

with open('./hind_limb_muscle_path_points.yaml', 'w') as outfile:
    yaml.dump(muscle_path_points, outfile, default_flow_style=False)
