#!/usr/bin/env python

"""This scripts parses an osim file."""

import xml.etree.ElementTree as ET
import argparse
import sys
import os
from jinja2 import Template, PackageLoader, Environment, FileSystemLoader


class Args(object):
    pass


parser = argparse.ArgumentParser("Parse osim file and generate python code")
parser.add_argument('-f', '--file', required=True, type=str,
                    help='File path of the osim file to parse')

parser.parse_args(namespace=Args)

#: Setup the template for osim-python
loader = FileSystemLoader(searchpath='templates')
env = Environment(loader=loader,
                  extensions=['jinja2.ext.do',
                              'jinja2.ext.loopcontrols'])
template = env.get_template('gen_muscles.py')

#: Read the osim file
tree = ET.parse(Args.file)
root = tree.getroot()

#: Loop over the muscles
muscles = []
for muscle in root.findall("Model/ForceSet/objects/Millard2012EquilibriumMuscle"):
    mus = {}
    mus['name'] = muscle.get("name")
    mus['path_points'] = []
    for _path in muscle.find('GeometryPath/PathPointSet/objects'):
        path_point = {}
        path_point['name'] = _path.get("name")
        path_point['location'] = list(map(float, _path.find('location').text.split(
            ' ')[1:]))
        path_point['body'] = _path.find('body').text
        mus['path_points'].append(path_point)
    mus['max_isometric_force'] = (
        float(muscle.find("max_isometric_force").text))
    mus['optimal_fiber_length'] = (
        float(muscle.find("optimal_fiber_length").text))
    mus['tendon_slack_length'] = (
        float(muscle.find("tendon_slack_length").text))
    mus['pennation_angle_at_optimal'] = (
        float(muscle.find("pennation_angle_at_optimal").text))
    muscles.append(mus)

#: Save jinja template for osim-python muscle
print(template.render(muscles=muscles))
