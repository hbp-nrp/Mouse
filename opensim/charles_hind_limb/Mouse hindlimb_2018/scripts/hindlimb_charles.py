#!/usr/bin/env python

""" Main file for mouse hindlimb simulation in opensim. """
import numpy as np

import biolog
from opensim_hindlimb_charles_gen import HindLimb
from osim_rl import OsimModel
import opensim as osim


def main():
    """ Main file. """
    #: osim model
    hindlimb = OsimModel('./hindlimb.osim',
                         visualize=True,
                         gravity=osim.Vec3(0, -9.81, 0))
    #: Initialize
    hindlimb.reset()
    hindlimb.reset_manager()

    #: List model components
    hindlimb.list_elements()

    #: Integrate
    for j in range(0, 50):
        hindlimb.integrate()


if __name__ == '__main__':
    main()
