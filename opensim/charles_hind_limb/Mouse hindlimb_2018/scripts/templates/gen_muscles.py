{%for muscle in muscles %}
##########: Add {{muscle.name}} muscle ##########

{{muscle.name}} = osim.Millard2012EquilibriumMuscle()
{{muscle.name}}.setName('{{muscle.name}}')
#: Muscle attachment

{%for path in muscle.path_points%}
{{muscle.name}}.addNewPathPoint(
    '{{path.name}}',
    self.model.getBodySet().get('{{path.body}}'),
    osim.Vec3({{path.location[0]}}*SCALE, {{path.location[1]}}*SCALE, {{path.location[2]}}*SCALE))
{%endfor%}
# Muscle parameters
{{muscle.name}}.setOptimalFiberLength({{muscle.optimal_fiber_length}}*SCALE)
{{muscle.name}}.setTendonSlackLength({{muscle.tendon_slack_length}}*SCALE)
{{muscle.name}}.setMaxIsometricForce({{muscle.max_isometric_force}})
self.model.addForce({{muscle.name}})


{%endfor%}
