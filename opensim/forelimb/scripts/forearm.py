#!/usr/bin/env python

""" Main file for mouse forearm simulation in opensim. """
import numpy as np

import biolog
from opensim_forearm_gen import ForearmModel
from osim_rl import OsimModel
import opensim as osim


def main():
    """ Main file. """
    #: osim model
    forearm = OsimModel('./forearm.osim',
                        visualize=True,
                        gravity=osim.Vec3(0, -9.81, 0))
    #: Initialize
    forearm.reset()
    forearm.reset_manager()

    #: List model components
    forearm.list_elements()

    #: Integrate
    for j in range(0, 5000):
        forearm.integrate()
        forearm.actuate(np.ones(
            forearm.muscleSet.getSize())*np.sin(2*np.pi*j*0.001*0.25))
        _ = forearm.compute_state_desc()


if __name__ == '__main__':
    main()
