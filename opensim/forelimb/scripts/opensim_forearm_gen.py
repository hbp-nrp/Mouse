#!/usr/bin/env python

""" This file generates the opensim forelimb model of the mouse. """

import opensim as osim
import numpy as np


class ForearmModel(object):
    """Class to create the mouse forearm model. """

    def __init__(self, model=None, name='forearm'):
        """Parameters
           ----------
           self: type
               description
           model: <osim.Model>
               Opensim model instance
           name: <str>
               Name of the forearm model
        """
        super(ForearmModel, self).__init__()
        self.model = model
        self.name = name

        if model is not None:
            self.model = model
        else:
            self.model = osim.Model()
        self.model.setName(name)

        self.body_length = 3.

        #: Add segments
        self.add_scapula()
        self.add_humerus()
        self.add_ulna()
        self.add_radius()
        self.add_hand()

        #: Generate body segments
        self.generate_body_segments()

        #: Add muscles
        # self.add_muscle()

        return

    def add_scapula(self):
        """ Add scapula(shoulderblade) segment to the simulation."""

        #: Thorax paramters
        _param = {}
        _param['mass'] = .2  # : Needs verification
        _param['length'] = 0.28*self.body_length
        _param['radius'] = 0.25*self.body_length*0.5
        _param['inertia'] = osim.Vec3()
        _param['inertia'][0] = (1/12.)*_param['mass']*(
            3*_param['radius']**2 + _param['length']**2)
        _param['inertia'][1] = (1/2.)*_param['mass'] * \
            _param['radius']**2
        _param['inertia'][2] = (1/12.)*_param['mass']*(
            3*_param['radius']**2 + _param['length']**2)
        #: Create scapula
        scapula = osim.Body('scapula',
                            _param['mass'], osim.Vec3(0),
                            osim.Inertia(_param['inertia']))
        shape = osim.Mesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/forelimb/Geometry/left_shoulderblade.stl')
        shape.set_scale_factors(osim.Vec3(.1))
        appearance = osim.Appearance()
        appearance.set_opacity = 0.5
        shape.set_Appearance(appearance)
        scapula.attachGeometry(shape)
        self.model.addBody(scapula)
        #: Add contact
        scapula_contact = osim.ContactMesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/forelimb/Geometry/left_shoulderblade.stl',
            osim.Vec3(0),
            osim.Vec3(0),
            scapula)
        scapula_contact.setName('scapula_contact')
        self.model.addContactGeometry(scapula_contact)
        return

    def add_humerus(self):
        """ Add humerus(shoulderblade) segment to the simulation."""

        #: Thorax paramters
        _param = {}
        _param['mass'] = .2  # : Needs verification
        _param['length'] = 0.28*self.body_length
        _param['radius'] = 0.25*self.body_length*0.5
        _param['inertia'] = osim.Vec3()
        _param['inertia'][0] = (1/12.)*_param['mass']*(
            3*_param['radius']**2 + _param['length']**2)
        _param['inertia'][1] = (1/2.)*_param['mass'] * \
            _param['radius']**2
        _param['inertia'][2] = (1/12.)*_param['mass']*(
            3*_param['radius']**2 + _param['length']**2)
        #: Create humerus
        humerus = osim.Body('humerus',
                            _param['mass'], osim.Vec3(0),
                            osim.Inertia(_param['inertia']))
        shape = osim.Mesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/forelimb/Geometry/left_fore_u.stl')
        shape.set_scale_factors(osim.Vec3(.1))
        appearance = osim.Appearance()
        appearance.set_opacity = 0.5
        shape.set_Appearance(appearance)
        humerus.attachGeometry(shape)
        self.model.addBody(humerus)
        #: Add contact
        humerus_contact = osim.ContactMesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/forelimb/Geometry/left_shoulderblade.stl',
            osim.Vec3(0),
            osim.Vec3(0),
            humerus)
        humerus_contact.setName('humerus_contact')
        self.model.addContactGeometry(humerus_contact)
        return

    def add_ulna(self):
        """ Add ulna segment to the simulation."""

        #: Thorax paramters
        _param = {}
        _param['mass'] = 0.20  # : Needs verification
        _param['length'] = 0.28*self.body_length
        _param['radius'] = 0.25*self.body_length*0.5
        _param['inertia'] = osim.Vec3()
        _param['inertia'][0] = (1/12.)*_param['mass']*(
            3*_param['radius']**2 + _param['length']**2)
        _param['inertia'][1] = (1/2.)*_param['mass'] * \
            _param['radius']**2
        _param['inertia'][2] = (1/12.)*_param['mass']*(
            3*_param['radius']**2 + _param['length']**2)
        #: Create ulna
        ulna = osim.Body('ulna',
                         _param['mass'], osim.Vec3(0),
                         osim.Inertia(_param['inertia']))
        shape = osim.Mesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/forelimb/Geometry/left_fore_ulna.stl')
        shape.set_scale_factors(osim.Vec3(.1))
        appearance = osim.Appearance()
        appearance.set_opacity = 0.5
        shape.set_Appearance(appearance)
        ulna.attachGeometry(shape)
        self.model.addBody(ulna)
        #: Add contact
        ulna_contact = osim.ContactMesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/forelimb/Geometry/left_fore_ulna.stl',
            osim.Vec3(0),
            osim.Vec3(0),
            ulna)
        ulna_contact.setName('ulna_contact')
        self.model.addContactGeometry(ulna_contact)
        return

    def add_radius(self):
        """ Add radius segment to the simulation."""

        #: Thorax paramters
        _param = {}
        _param['mass'] = 0.2  # : Needs verification
        _param['length'] = 0.28*self.body_length
        _param['radius'] = 0.25*self.body_length*0.5
        _param['inertia'] = osim.Vec3()
        _param['inertia'][0] = (1/12.)*_param['mass']*(
            3*_param['radius']**2 + _param['length']**2)
        _param['inertia'][1] = (1/2.)*_param['mass'] * \
            _param['radius']**2
        _param['inertia'][2] = (1/12.)*_param['mass']*(
            3*_param['radius']**2 + _param['length']**2)
        #: Create radius
        radius = osim.Body('radius',
                           _param['mass'], osim.Vec3(0),
                           osim.Inertia(_param['inertia']))
        shape = osim.Mesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/forelimb/Geometry/left_fore_radius.stl')
        shape.set_scale_factors(osim.Vec3(.1))
        appearance = osim.Appearance()
        appearance.set_opacity = 0.5
        shape.set_Appearance(appearance)
        radius.attachGeometry(shape)
        self.model.addBody(radius)
        #: Add contact
        radius_contact = osim.ContactMesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/forelimb/Geometry/left_fore_radius.stl',
            osim.Vec3(0),
            osim.Vec3(0),
            radius)
        radius_contact.setName('radius_contact')
        self.model.addContactGeometry(radius_contact)
        return

    def add_hand(self):
        """ Add hand segment to the simulation."""

        #: Thorax paramters
        _param = {}
        _param['mass'] = .2  # : Needs verification
        _param['length'] = 0.1*self.body_length
        _param['radius'] = 0.25*self.body_length*0.5
        _param['inertia'] = osim.Vec3()
        _param['inertia'][0] = (1/12.)*_param['mass']*(
            3*_param['radius']**2 + _param['length']**2)
        _param['inertia'][1] = (1/2.)*_param['mass'] * \
            _param['radius']**2
        _param['inertia'][2] = (1/12.)*_param['mass']*(
            3*_param['radius']**2 + _param['length']**2)
        #: Create hand
        hand = osim.Body('hand',
                         _param['mass'], osim.Vec3(0, 0., 0),
                         osim.Inertia(_param['inertia']))
        shape = osim.Mesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/forelimb/Geometry/left_fore_paw.stl')
        shape.set_scale_factors(osim.Vec3(.1))
        appearance = osim.Appearance()
        appearance.set_opacity = 0.5
        shape.set_Appearance(appearance)
        hand.attachGeometry(shape)
        self.model.addBody(hand)
        #: Add contact
        hand_contact = osim.ContactMesh(
            '/Users/tatarama/Documents/EPFL-PhD/Projects/BioRobAnimals/mouse/modeling/opensim/forelimb/Geometry/left_fore_l.stl',
            osim.Vec3(0),
            osim.Vec3(0),
            hand)
        hand_contact.setName('hand_contact')
        self.model.addContactGeometry(hand_contact)
        return

    def generate_body_segments(self):
        """
        Connect the segments in the body.
        Thorax->Abdomen->Head
        Parameters
        ----------
        """
        #: Create joints
        #: Ground-Scapula
        #: pylint: disable=no-member
        ground_scapula = osim.WeldJoint(
            'ground_scapula', self.model.getGround(),
            osim.Vec3(0, 2.5, 0),
            osim.Vec3(0),
            self.model.getBodySet().get('scapula'),
            osim.Vec3(0),
            osim.Vec3(0, 0, 0))

        #: Scapula-Humerus
        #: pylint: disable=no-member
        scapula_humerus = osim.BallJoint(
            'scapula_humerus',
            self.model.getBodySet().get('scapula'),
            osim.Vec3(0, 0, -.329),
            osim.Vec3(0),
            self.model.getBodySet().get('humerus'),
            osim.Vec3(0, .372,  0),
            osim.Vec3(0, 0, 0))

        scapula_humerus.get_coordinates(0).set_locked(True)
        scapula_humerus.get_coordinates(1).set_locked(True)

        spatial_trans = osim.SpatialTransform()

        #: Set transformation axes
        trans = spatial_trans.updTransformAxis(0)
        trans.append_coordinates("Elbow_Ry")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(1)
        trans.append_coordinates("Elbow_Rz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(2)
        trans.append_coordinates("Elbow_Rx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(3)
        trans.append_coordinates("Elbow_Tx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(4)
        trans.append_coordinates("Elbow_Ty")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(5)
        trans.append_coordinates("Elbow_Tz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())

        #: Humerus-Ulna
        #: pylint: disable=no-member
        humerus_ulna = osim.CustomJoint(
            'humerus_ulna',
            self.model.getBodySet().get('humerus'),
            osim.Vec3(0, -0.41, 0),
            osim.Vec3(0, 0, 0),
            self.model.getBodySet().get('ulna'),
            osim.Vec3(0, 0, .329),
            osim.Vec3(0, np.pi/2., 0), spatial_trans)
        #: Set coordinates
        humerus_ulna.set_coordinates(0,
                                     osim.Coordinate(
                                         "Elbow_Ry",
                                         1, 0, -3.141, 3.141))
        humerus_ulna.get_coordinates(0).set_clamped(False)
        humerus_ulna.get_coordinates(0).set_locked(True)
        humerus_ulna.set_coordinates(1,
                                     osim.Coordinate(
                                         "Elbow_Rz",
                                         1, 0.0, -np.pi/4, np.pi/4))
        humerus_ulna.get_coordinates(1).set_clamped(True)
        humerus_ulna.get_coordinates(1).set_locked(False)
        humerus_ulna.set_coordinates(2,
                                     osim.Coordinate(
                                         "Elbow_Rx",
                                         1, 0, -3.141, 3.141))
        humerus_ulna.get_coordinates(2).set_clamped(False)
        humerus_ulna.get_coordinates(2).set_locked(True)
        humerus_ulna.set_coordinates(3,
                                     osim.Coordinate(
                                         "Elbow_Tx",
                                         2, 0, -100, 100))
        humerus_ulna.get_coordinates(3).set_clamped(False)
        humerus_ulna.get_coordinates(3).set_locked(True)
        humerus_ulna.set_coordinates(4,
                                     osim.Coordinate(
                                         "Elbow_Ty",
                                         2, 0, -100, 100))
        humerus_ulna.get_coordinates(4).set_clamped(False)
        humerus_ulna.get_coordinates(4).set_locked(True)
        humerus_ulna.set_coordinates(5,
                                     osim.Coordinate(
                                         "Elbow_Tz",
                                         2, 0, -1, 1))
        humerus_ulna.get_coordinates(5).set_clamped(False)
        humerus_ulna.get_coordinates(5).set_locked(True)

        spatial_trans = osim.SpatialTransform()

        #: Set transformation axes
        trans = spatial_trans.updTransformAxis(0)
        trans.append_coordinates("UlnarRadial_Ry")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(1)
        trans.append_coordinates("UlnarRadial_Rz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(2)
        trans.append_coordinates("UlnarRadial_Rx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(3)
        trans.append_coordinates("UlnarRadial_Tx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(4)
        trans.append_coordinates("UlnarRadial_Ty")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(5)
        trans.append_coordinates("UlnarRadial_Tz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())

        #: Ulna-Radius
        #: pylint: disable=no-member
        ulna_radius = osim.CustomJoint(
            'ulna_radius',
            self.model.getBodySet().get('ulna'),
            osim.Vec3(0, 0.05, 0.329),
            osim.Vec3(0, 0, 0),
            self.model.getBodySet().get('radius'),
            osim.Vec3(0, 0.05, 0.329),
            osim.Vec3(0, 0, 0), spatial_trans)

        #: Set coordinates
        ulna_radius.set_coordinates(0,
                                    osim.Coordinate(
                                        "UlnarRadial_Ry",
                                        1, 0, -3.141, 3.141))
        ulna_radius.get_coordinates(0).set_locked(True)
        ulna_radius.set_coordinates(1,
                                    osim.Coordinate(
                                        "UlnarRadial_Rz",
                                        1, 0., -np.pi/4, np.pi/4))
        ulna_radius.get_coordinates(1).set_locked(False)
        ulna_radius.set_coordinates(2,
                                    osim.Coordinate(
                                        "UlnarRadial_Rx",
                                        1, 0, -3.141, 3.141))
        ulna_radius.get_coordinates(2).set_locked(True)
        ulna_radius.set_coordinates(3,
                                    osim.Coordinate(
                                        "UlnarRadial_Tx",
                                        2, 0, -1, 1))
        ulna_radius.get_coordinates(3).set_locked(True)
        ulna_radius.set_coordinates(4,
                                    osim.Coordinate(
                                        "UlnarRadial_Ty",
                                        2, 0, -1, 1))
        ulna_radius.get_coordinates(4).set_locked(True)
        ulna_radius.set_coordinates(5,
                                    osim.Coordinate(
                                        "UlnarRadial_Tz",
                                        2, 0, -1, 1))
        ulna_radius.get_coordinates(5).set_locked(True)

        spatial_trans = osim.SpatialTransform()

        #: Set transformation axes
        trans = spatial_trans.updTransformAxis(0)
        trans.append_coordinates("RadiusHand_Ry")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(1)
        trans.append_coordinates("RadiusHand_Rz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(2)
        trans.append_coordinates("RadiusHand_Rx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(3)
        trans.append_coordinates("RadiusHand_Tx")
        trans.set_axis(0, osim.Vec3(1, 0, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(4)
        trans.append_coordinates("RadiusHand_Ty")
        trans.set_axis(0, osim.Vec3(0, 1, 0))
        trans.set_function(0, osim.LinearFunction())
        trans = spatial_trans.updTransformAxis(5)
        trans.append_coordinates("RadiusHand_Tz")
        trans.set_axis(0, osim.Vec3(0, 0, 1))
        trans.set_function(0, osim.LinearFunction())

        #: Radius-Hand
        #: pylint: disable=no-member
        radius_hand = osim.CustomJoint(
            'radius_hand',
            self.model.getBodySet().get('radius'),
            osim.Vec3(0.0, -0.05, -0.5),
            osim.Vec3(0, 0, 0),
            self.model.getBodySet().get('hand'),
            osim.Vec3(0.0, -0.25, 0.),
            osim.Vec3(0, 0, 0),
            spatial_trans)

        #: Set coordinates
        radius_hand.set_coordinates(0,
                                    osim.Coordinate(
                                        "RadiusHand_Ry",
                                        1, 0, -3.141, 3.141))
        radius_hand.get_coordinates(0).set_locked(True)
        radius_hand.set_coordinates(1,
                                    osim.Coordinate(
                                        "RadiusHand_Rz",
                                        1, 0., -3.141, 3.141))
        radius_hand.get_coordinates(1).set_locked(True)
        radius_hand.set_coordinates(2,
                                    osim.Coordinate(
                                        "RadiusHand_Rx",
                                        1, -np.pi/2, -3.141, 3.141))
        radius_hand.get_coordinates(2).set_locked(False)
        radius_hand.set_coordinates(3,
                                    osim.Coordinate(
                                        "RadiusHand_Tx",
                                        2, 0, -1, 1))
        radius_hand.get_coordinates(3).set_locked(True)
        radius_hand.set_coordinates(4,
                                    osim.Coordinate(
                                        "RadiusHand_Ty",
                                        2, 0, -1, 1))
        radius_hand.get_coordinates(4).set_locked(True)
        radius_hand.set_coordinates(5,
                                    osim.Coordinate(
                                        "RadiusHand_Tz",
                                        2, 0, -1, 1))
        radius_hand.get_coordinates(5).set_locked(True)

        #: Add joints to the model
        self.model.addJoint(ground_scapula)
        self.model.addJoint(scapula_humerus)
        self.model.addJoint(humerus_ulna)
        self.model.addJoint(ulna_radius)
        self.model.addJoint(radius_hand)

    def add_muscle(self):
        """Add muscle and wrapping objects"""

        #: Add biceps_brachii_longus muscle
        bbl = osim.Millard2012EquilibriumMuscle()
        bbl.setName('biceps_brachii_longus')
        # Muscle attachment
        bbl.addNewPathPoint(
            'origin',
            self.model.getBodySet().get('scapula'),
            osim.Vec3(0.081, 0.044, -.28))
        # bbl.addNewPathPoint(
        #     'mid',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.06, 0.372, 0.0))
        bbl.addNewPathPoint(
            'insertion',
            self.model.getBodySet().get('ulna'),
            osim.Vec3(0.0, 0.05, 0.11))
        # Muscle parameters
        bbl.setOptimalFiberLength(0.55)
        bbl.setTendonSlackLength(0.3)
        bbl.setMaxIsometricForce(50)
        self.model.addForce(bbl)

        # #: Add biceps_brachii_shorthead muscle
        # bbs = osim.Millard2012EquilibriumMuscle()
        # bbs.setName('biceps_brachii_short')
        # # Muscle attachment
        # bbs.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('scapula'),
        #     osim.Vec3(0.1, -0.1, -.28))
        # bbs.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(-0.05, 0.04, 0.25))
        # # Muscle parameters
        # bbs.setOptimalFiberLength(0.5555)
        # bbs.setTendonSlackLength(0.3)
        # bbs.setMaxIsometricForce(10000)
        # self.model.addForce(bbs)

        # #: Add brachialis
        # bra = osim.Millard2012EquilibriumMuscle()
        # bra.setName('brachialis')
        # # Muscle attachment
        # bra.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.0, .0))
        # bra.addNewPathPoint(
        #     'mid',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # bra.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # # Muscle parameters
        # bra.setOptimalFiberLength(0.5555)
        # bra.setTendonSlackLength(0.3)
        # bra.setMaxIsometricForce(10000)
        # self.model.addForce(bra)

        # #: Add coracobrachialis
        # cor = osim.Millard2012EquilibriumMuscle()
        # cor.setName('coracobrachialis')
        # # Muscle attachment
        # cor.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('scapula'),
        #     osim.Vec3(0.081, 0.044, -.28))
        # cor.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # # Muscle parameters
        # cor.setOptimalFiberLength(0.5555)
        # cor.setTendonSlackLength(0.3)
        # cor.setMaxIsometricForce(10000)
        # self.model.addForce(cor)

        # #: Add deb
        # deb = osim.Millard2012EquilibriumMuscle()
        # deb.setName('dorso_eitrochlearis_brachii')
        # # Muscle attachment
        # deb.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # deb.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.0, 0.0, 0.25))
        # # Muscle parameters
        # deb.setOptimalFiberLength(0.5555)
        # deb.setTendonSlackLength(0.3)
        # deb.setMaxIsometricForce(10000)
        # self.model.addForce(deb)

        # #: Add extensor_carpi_radialis_brevis
        # ecr_b = osim.Millard2012EquilibriumMuscle()
        # ecr_b.setName('extensor_carpi_radialis_brevis')
        # # Muscle attachment
        # ecr_b.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # ecr_b.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # # Muscle parameters
        # ecr_b.setOptimalFiberLength(0.5555)
        # ecr_b.setTendonSlackLength(0.3)
        # ecr_b.setMaxIsometricForce(10000)
        # self.model.addForce(ecr_b)

        # #: Add extensor_carpi_radialis_longus
        # ecr_l = osim.Millard2012EquilibriumMuscle()
        # ecr_l.setName('extensor_carpi_radialis_longus')
        # # Muscle attachment
        # ecr_l.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.0, 0.2, 0.0))
        # ecr_l.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # # Muscle parameters
        # ecr_l.setOptimalFiberLength(0.5555)
        # ecr_l.setTendonSlackLength(0.3)
        # ecr_l.setMaxIsometricForce(10000)
        # self.model.addForce(ecr_l)

        # #: Add extensor_carpi_ulnaris
        # ecu = osim.Millard2012EquilibriumMuscle()
        # ecu.setName('extensor_carpi_ulnaris')
        # # Muscle attachment
        # ecu.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.39, 0.0))
        # ecu.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.0, 0.0, 0.25))
        # # Muscle parameters
        # ecu.setOptimalFiberLength(0.5555)
        # ecu.setTendonSlackLength(0.3)
        # ecu.setMaxIsometricForce(10000)
        # self.model.addForce(ecu)

        # #: Add extensor_digitorum_communis
        # edc = osim.Millard2012EquilibriumMuscle()
        # edc.setName('extensor_digitorum_communis')
        # # Muscle attachment
        # edc.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, -0.39, 0.0))
        # edc.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # # Muscle parameters
        # edc.setOptimalFiberLength(0.5555)
        # edc.setTendonSlackLength(0.3)
        # edc.setMaxIsometricForce(10000)
        # self.model.addForce(edc)

        # #: Add extensor_digitorum_lateralis
        # edl = osim.Millard2012EquilibriumMuscle()
        # edl.setName('extensor_digitorum_lateralis')
        # # Muscle attachment
        # edl.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, -0.39, 0.0))
        # edl.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(-0.1, 0.0, 0.0))
        # # Muscle parameters
        # edl.setOptimalFiberLength(0.5555)
        # edl.setTendonSlackLength(0.3)
        # edl.setMaxIsometricForce(10000)
        # self.model.addForce(edl)

        # #: Add extensor_digitorum_lateralis
        # edl = osim.Millard2012EquilibriumMuscle()
        # edl.setName('extensor_digitorum_lateralis')
        # # Muscle attachment
        # edl.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, -0.39, 0.0))
        # edl.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(-0.1, 0.0, 0.0))
        # # Muscle parameters
        # edl.setOptimalFiberLength(0.5555)
        # edl.setTendonSlackLength(0.3)
        # edl.setMaxIsometricForce(10000)
        # self.model.addForce(edl)

        # #: Add extensor_indicis_proprius
        # eip = osim.Millard2012EquilibriumMuscle()
        # eip.setName('extensor_indicis_proprius')
        # # Muscle attachment
        # eip.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # eip.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.1, 0.0, 0.0))
        # # Muscle parameters
        # eip.setOptimalFiberLength(0.5555)
        # eip.setTendonSlackLength(0.3)
        # eip.setMaxIsometricForce(10000)
        # self.model.addForce(eip)

        # #: Add flexor_carpi_radialis
        # fcr = osim.Millard2012EquilibriumMuscle()
        # fcr.setName('flexor_carpi_radialis')
        # # Muscle attachment
        # fcr.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, -0.39, 0.0))
        # fcr.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(-0.1, -0.2, 0.))
        # # Muscle parameters
        # fcr.setOptimalFiberLength(0.5555)
        # fcr.setTendonSlackLength(0.3)
        # fcr.setMaxIsometricForce(10000)
        # self.model.addForce(fcr)

        # #: Add flexor_carpi_ulnaris
        # fcu = osim.Millard2012EquilibriumMuscle()
        # fcu.setName('flexor_carpi_ulnaris')
        # # Muscle attachment
        # fcu.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, -0.39, 0.0))
        # fcu.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.1, -0.2, 0.))
        # # Muscle parameters
        # fcu.setOptimalFiberLength(0.5555)
        # fcu.setTendonSlackLength(0.3)
        # fcu.setMaxIsometricForce(10000)
        # self.model.addForce(fcu)

        # #: Add flexor_digitorum_profundus_radial_head
        # fdprh = osim.Millard2012EquilibriumMuscle()
        # fdprh.setName('flexor_digitorum_profundus_radial_head')
        # # Muscle attachment
        # fdprh.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # fdprh.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.0, -0.2, 0.))
        # # Muscle parameters
        # fdprh.setOptimalFiberLength(0.5555)
        # fdprh.setTendonSlackLength(0.3)
        # fdprh.setMaxIsometricForce(10000)
        # self.model.addForce(fdprh)

        # #: Add flexor_digitorum_profundus_superficial_head
        # fdpsh = osim.Millard2012EquilibriumMuscle()
        # fdpsh.setName('flexor_digitorum_profundus_superficial_head')
        # # Muscle attachment
        # fdpsh.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # fdpsh.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.0, -0.2, 0.))
        # # Muscle parameters
        # fdpsh.setOptimalFiberLength(0.5555)
        # fdpsh.setTendonSlackLength(0.3)
        # fdpsh.setMaxIsometricForce(10000)
        # self.model.addForce(fdpsh)

        # #: Add flexor_digitorum_profundus_ulnar_head
        # fdpuh = osim.Millard2012EquilibriumMuscle()
        # fdpuh.setName('flexor_digitorum_profundus_ulnar_head')
        # # Muscle attachment
        # fdpuh.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # fdpuh.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.0, -0.25, 0.))
        # # Muscle parameters
        # fdpuh.setOptimalFiberLength(0.5555)
        # fdpuh.setTendonSlackLength(0.3)
        # fdpuh.setMaxIsometricForce(10000)
        # self.model.addForce(fdpuh)

        # #: Add flexor_digitorum_superficialis_1
        # fds1 = osim.Millard2012EquilibriumMuscle()
        # fds1.setName('flexor_digitorum_superficialis_1')
        # # Muscle attachment
        # fds1.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # fds1.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.05, -0.25, 0.))
        # # Muscle parameters
        # fds1.setOptimalFiberLength(0.5555)
        # fds1.setTendonSlackLength(0.3)
        # fds1.setMaxIsometricForce(10000)
        # self.model.addForce(fds1)

        # #: Add flexor_digitorum_superficialis_2
        # fds2 = osim.Millard2012EquilibriumMuscle()
        # fds2.setName('flexor_digitorum_superficialis_2')
        # # Muscle attachment
        # fds2.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.0, 0.0))
        # fds2.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(-0.05, -0.25, 0.))
        # # Muscle parameters
        # fds2.setOptimalFiberLength(0.5555)
        # fds2.setTendonSlackLength(0.3)
        # fds2.setMaxIsometricForce(10000)
        # self.model.addForce(fds2)

        # #: Add palmaris_longus
        # pal = osim.Millard2012EquilibriumMuscle()
        # pal.setName('palmaris_longus')
        # # Muscle attachment
        # pal.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0.0, 0.39, 0.0))
        # pal.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.0, 0., 0.))
        # # Muscle parameters
        # pal.setOptimalFiberLength(0.5555)
        # pal.setTendonSlackLength(0.3)
        # pal.setMaxIsometricForce(10000)
        # self.model.addForce(pal)

        # #: Add pronator_quadratus
        # pro_quad = osim.Millard2012EquilibriumMuscle()
        # pro_quad.setName('pronator_quadratus')
        # # Muscle attachment
        # pro_quad.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.1, 0.0, 0.0))
        # pro_quad.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('hand'),
        #     osim.Vec3(0.0, 0.25, 0.))
        # # Muscle parameters
        # pro_quad.setOptimalFiberLength(0.5555)
        # pro_quad.setTendonSlackLength(0.3)
        # pro_quad.setMaxIsometricForce(10000)
        # self.model.addForce(pro_quad)

        # #: Add pronator_teres
        # pro_teres = osim.Millard2012EquilibriumMuscle()
        # pro_teres.setName('pronator_teres')
        # # Muscle attachment
        # pro_teres.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0., -0.39, 0.0))
        # pro_teres.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.0, 0.25, 0.))
        # # Muscle parameters
        # pro_teres.setOptimalFiberLength(0.5555)
        # pro_teres.setTendonSlackLength(0.3)
        # pro_teres.setMaxIsometricForce(10000)
        # self.model.addForce(pro_teres)

        # #: Add triceps_brachii_lateral
        # tb_lat = osim.Millard2012EquilibriumMuscle()
        # tb_lat.setName('triceps_brachii_lateral')
        # # Muscle attachment
        # tb_lat.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0., -0.39, 0.1))
        # tb_lat.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.0, 0.25, 0.))
        # # Muscle parameters
        # tb_lat.setOptimalFiberLength(0.5555)
        # tb_lat.setTendonSlackLength(0.3)
        # tb_lat.setMaxIsometricForce(10000)
        # self.model.addForce(tb_lat)

        # #: Add triceps_brachii_long
        # tb_long = osim.Millard2012EquilibriumMuscle()
        # tb_long.setName('triceps_brachii_long')
        # # Muscle attachment
        # tb_long.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('scapula'),
        #     osim.Vec3(0., 0.2, 0.1))
        # tb_long.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.0, 0.25, 0.))
        # # Muscle parameters
        # tb_long.setOptimalFiberLength(0.5555)
        # tb_long.setTendonSlackLength(0.3)
        # tb_long.setMaxIsometricForce(10000)
        # self.model.addForce(tb_long)

        # #: Add triceps_brachii_medial
        # tb_med = osim.Millard2012EquilibriumMuscle()
        # tb_med.setName('triceps_brachii_medial')
        # # Muscle attachment
        # tb_med.addNewPathPoint(
        #     'origin',
        #     self.model.getBodySet().get('humerus'),
        #     osim.Vec3(0., -0.39, 0.1))
        # tb_med.addNewPathPoint(
        #     'insertion',
        #     self.model.getBodySet().get('elbow'),
        #     osim.Vec3(0.0, 0.2, 0.))
        # # Muscle parameters
        # tb_med.setOptimalFiberLength(0.5555)
        # tb_med.setTendonSlackLength(0.3)
        # tb_med.setMaxIsometricForce(10000)
        # self.model.addForce(tb_med)

        # #: Add biceps_brachii_longus muscle wrapping
        # bbl_wrap = osim.WrapCylinder()
        # bbl_wrap.setName(
        #     'biceps_brachii_longus_wrap')
        # bbl_wrap.set_translation(
        #     osim.Vec3(0.))
        # bbl_wrap.set_xyz_body_rotation(
        #     osim.Vec3(0, np.pi/2, 0))
        # bbl_wrap.set_length(0.2)
        # bbl_wrap.set_radius(0.12)
        # bbl_wrap.set_quadrant('-y')
        # self.model.getBodySet().get('humerus').addWrapObject(
        #     bbl_wrap)
        # bbl.updGeometryPath().addPathWrap(bbl_wrap)

        # #: Add biceps_brachii_short muscle wrapping
        # bbs_wrap = osim.WrapCylinder()
        # bbs_wrap.setName(
        #     'biceps_brachii_short_wrap')
        # bbs_wrap.set_translation(
        #     osim.Vec3(0.))
        # bbs_wrap.set_xyz_body_rotation(
        #     osim.Vec3(0, np.pi/2, 0))
        # bbs_wrap.set_length(0.2)
        # bbs_wrap.set_radius(0.12)
        # bbs_wrap.set_quadrant('-y')
        # self.model.getBodySet().get('humerus').addWrapObject(
        #     bbs_wrap)
        # bbs.updGeometryPath().addPathWrap(bbs_wrap)

    def show_physical_properties(self):
        """ Print physical properties of every segment in the foremarm."""

        for body in self.model.getBodySet():
            _mass = body.get_mass()
            _name = body.getName()
            _inertia = []
            for j in range(0, 3):
                _inertia.append(body.get_inertia().get(j))
            print('Name : {}'.format(_name))
            print('\t Mass : {}'.format(_mass))
            print('\t Inertia : {}'.format(_inertia))

        return


def main():
    """ Main function. """

    #: osim model
    model = osim.Model()
    forearm = ForearmModel(model)
    forearm.add_muscle()
    # forearm.init_arm_position()
    forearm.show_physical_properties()

    #: Dump osim file
    file_name = './forearm.osim'
    model.printToXML(file_name)


if __name__ == '__main__':
    main()
