"""Generate a mouse model."""

import imp
import os
import sys
import numpy as np
import xml.dom.minidom
from xml.etree import ElementTree as etree

import bpy

try:
    from biorob_blender_modeling import armature, mesh
except ImportError:
    sys.path.append(
        os.path.join(os.getenv("HOME"),
                     'Stata-PhD/Projects/BioRobAnimals/mouse/modeling/'
                     'blender/'))
    from biorob_blender_modeling import armature, mesh


imp.reload(armature)
imp.reload(mesh)

HOME = os.getenv("HOME")
ROOT_DIR = os.path.join(HOME,
                        'Stata-PhD/Projects/BioRobAnimals/mouse/modeling/blender/')
EXPORT = False
SCALE = 1

#: Clear the world
print("Clearing world!")
armature.clear_world()

#: Make bone table for double pendulum
#: base, link1, link2
origin = (0, -12.5825, 5.27158)

bone_table = armature.read_rig(os.path.join(ROOT_DIR,
                                            'conf/mouse_skeleton_rig.csv'))

armature_gen = armature.create_rig(name='double', origin=origin,
                                   bone_table=bone_table, scale=SCALE)
#: Generate meshes
mesh.read(mesh_path=os.path.join(ROOT_DIR,
                                 'meshes/stl/'),
          mesh_type='stl', scale=SCALE)

# #: Set and Edit kinematics
# bpy.data.objects['Armature'].pose.bones["LFemur"].rotation_mode = 'XYZ'
# bpy.data.objects['Armature'].pose.bones["LFemur"].rotation_euler.rotate_axis(
#     'X', 1.575)

############### Generate Gazebo Model ###############
#: Select an object to switch to object mode
_obj = bpy.data.objects[0]
bpy.context.scene.objects.active = _obj
_obj.select = True
#: Switch to object mode
bpy.ops.object.mode_set(mode='OBJECT')
#: Unselect all objects
bpy.ops.object.select_all(action='DESELECT')

#: Select the Robot Model
bpy.ops.roboteditor.selectarmature(model_name='Armature')

#: Reimport all bones
bpy.ops.roboteditor.importnative()

#: Attach visuals and collisions to the armature
bpy.data.scenes['Scene'].RobotDesigner.display_mesh_selection = 'visual'
for name, obj in bpy.data.objects.items():
    if obj.type == 'MESH':
        print('Attaching mesh -> {}'.format(name))
        bpy.ops.roboteditor.select_segment(segment_name=name)
        bpy.ops.roboteditor.select_geometry(geometry_name=name)
        #: Attach the object with visual
        bpy.ops.roboteditor.assign_geometry(
            attach_collision_geometry=False)
        #: Generate Convex hull collision shapes
        bpy.ops.roboteditor.generateconvexhull()

#: Switch to object mode
bpy.context.scene.objects.active = bpy.data.objects['Armature']
bpy.data.objects['Armature'].select = True
bpy.ops.object.mode_set(mode='POSE')
bpy.ops.pose.select_all(action='SELECT')

#: Attach and Generate Physics shapes
bpy.ops.roboteditor.createphysicsframe()
bpy.ops.roboteditor.computephysicsframe(density=0.125,
                                        from_visual_geometry=False)

bpy.context.scene.objects.active = bpy.data.objects['Armature']
bpy.data.objects['Armature'].select = True
bpy.ops.object.mode_set(mode='OBJECT')
bpy.ops.object.select_all(action='DESELECT')

for (name, bone) in bpy.data.armatures['double'].bones.items():
    print('Editing kinematics of bone -> {}'.format(name))
    try:
        bone['RobotEditor']['jointMode'] = 0
        bone['RobotEditor']['axis'] = 0
    except KeyError:
        print('{} does not have a joint type'.format(name))

########### SET INIT POSITIONS ##########
_obj = bpy.data.objects['Armature']
bpy.context.scene.objects.active = _obj
_obj.select = True
#: Switch to object mode
bpy.ops.object.mode_set(mode='POSE')

for (name, bone) in bpy.data.armatures['double'].bones.items():
    print('Set init position of bone -> {}'.format(name))
    bpy.data.objects['Armature'].pose.bones[name].rotation_mode = 'XYZ'
    bpy.data.objects['Armature'].pose.bones[name].rotation_euler.x \
        = np.deg2rad(
            -1*bone['RobotEditor']['Euler']['alpha'].values()[0])
    bpy.data.objects['Armature'].pose.bones[name].rotation_euler.y \
        = np.deg2rad(
            -1*bone['RobotEditor']['Euler']['beta'].values()[0])
    bpy.data.objects['Armature'].pose.bones[name].rotation_euler.z \
        = np.deg2rad(
        -1*bone['RobotEditor']['Euler']['gamma'].values()[0])


if(EXPORT):
    path_model = '/home/tatarama/.gazebo/models/mouse'

    bpy.ops.roboteditor.export_to_sdf_package(filepath=path_model,
                                              gazebo=True)

    path_sdf = os.path.join(path_model, 'model.sdf')

    with open(path_sdf) as model:
        xml_string = model.read()

    # xml_string = etree.tostring(root).decode()
    xml_string = xml_string.replace("\n", "")
    while "  " in xml_string:
        xml_string = xml_string.replace("  ", "")

    # xml_string = xml_string.replace(
    #     "file://meshes",
    #     "file:///home/tatarama/Desktop/test_nrp_urdf/meshes")
    # xml_string = xml_string.replace(
    #     "file://collisions",
    #     "file:///home/tatarama/Desktop/test_nrp_urdf/collisions")

    xml_text = xml.dom.minidom.parseString(xml_string)
    with open(path_sdf, "w+") as sdf_file:
        sdf_file.write(str(xml_text.toprettyxml(
            indent="  ",
            newl='\n'
            # encoding="UTF-8"
        )))
