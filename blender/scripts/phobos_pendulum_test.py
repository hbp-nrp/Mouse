"""Test file for generating a two link pendulum model with phobos.

https://en.blender.org/index.php/Dev:Py/Scripts/Cookbook/Code_snippets/Armatures

"""
import math
import os
import sys

import pandas as pd

import bpy
from mathutils import Euler, Matrix, Vector

########## RIGGING ##########
#: Clearing the world


def clear_world():
    """Clear all the elements in a blender world except lamp & Camera"""

    #: Select an object to switch to object mode
    _obj = bpy.data.objects[0]
    bpy.context.scene.objects.active = _obj
    _obj.select = True

    #: Switch to object mode
    bpy.ops.object.mode_set(mode='OBJECT')

    #: Unselect all objects
    bpy.ops.object.select_all(action='DESELECT')

    #: Clear all elements
    for object in bpy.data.objects:
        if object.name in ['Camera', 'Lamp']:
            pass
        else:
            print('[DELETING] {} -> {}'.format(object.name,
                                               object.type))
            bpy.context.scene.objects.active = object
            object.select = True
            #: Switch to object mode
            # bpy.ops.object.mode_set(mode='OBJECT')
            bpy.ops.object.delete()
            bpy.ops.object.select_all(action='DESELECT')


def create_rig(name, origin, bone_table):
    """Create a blender rig
        Parameters
        ----------
        name: <str>
            Name of the rig/armature
        origin: <tuple>
            Origin of the root joint.
        bone_table: <list>
            Each element of the list containing the following :
            1. bone_name : <str>
            2. parent_name : <str>
            3. vector : <Vector>
    """

    #: Create an armature and an object
    bpy.ops.object.add(type='ARMATURE',
                       enter_editmode=True,
                       location=origin)

    #: Initialize the armature
    obj = bpy.context.object
    obj.show_x_ray = True
    obj.name = 'Armature'
    armature = obj.data
    armature.name = name
    armature.show_axes = True

    #: Creating bones
    #: Switch to EDIT Mode for bone generation
    bpy.ops.object.mode_set(mode='EDIT')
    #: Loop over the table of bones
    for (bone_name, parent_name, location) in bone_table:
        #: Create the bone
        bone = armature.edit_bones.new(bone_name)
        if parent_name:
            parent = armature.edit_bones[parent_name]
            #: Attach the parent to the current bone
            bone.parent = parent
            bone.head = parent.tail
            bone.use_connect = False
            (_, rot, _) = parent.matrix.decompose()
        else:
            bone.head = (0., 0., 0.)
            rot = Matrix.Translation((0., 0., 0.))
        bone.tail = rot*Vector(location) + bone.head
    #: Switch back to OBJECT mode
    bpy.ops.object.mode_set(mode='OBJECT')
    return obj


#: Set pose of the rig
def pose_rig(rig, pose_table):
    """Set the pose of a given rig.
    Parameters
    ----------
    rig: <ARMATURE>
        Blender object of type armature
    pose_table: <list>
        List containing pose of each bone.
        1. Name <str> : Name of the bone
        2. Angles <tuple> : (X, Y, Z) EULER rotations.
    """
    #: Make the given rig active
    bpy.context.scene.objects.active = rig

    #: Set the mode to POSE
    bpy.ops.object.mode_set(mode='POSE')

    #: Loop over the poses in pose table
    for (bone_name, angles) in pose_table:
        bone = rig.pose.bones[bone_name]
        #: Change the mode EULER Rotations
        bone.rotation_mode = 'XYZ'

        #: Set the rotation
        bone.rotation_euler.rotate(Euler(Vector(
            tuple(math.radians(rad) for rad in angles))))

    #: Revert back to OBJECT mode
    bpy.ops.object.mode_set(mode='OBJECT')


#: Save rig
def save_rig(rig, file_path=bpy.path.abspath('//'), file_name=None):
    """Save the rig to a file.
    Parameters
    ----------
    rig: <ARMATURE>
        Rig/Armature to be saved in a csv format
    file_path: <str>
        Location to save the file. By default use current folder
    file_name: <str>
        Name of the rig file. *.csv. By default use Armature name.
    """
    #: File attributes
    if file_name is None:
        file_name = rig.name + '.csv'

    path = os.path.join(file_path, file_name)

    #: Make the given rig active
    bpy.context.scene.objects.active = rig

    #: Set the mode to EDIT
    bpy.ops.object.mode_set(mode='EDIT')

    #: List to store the data
    bone_table_header = ['bone_name', 'parent', 'location']
    bone_table = []

    #: Loop over the table of bones
    for (name, bone) in rig.data.edit_bones.items():
        bone_table.append(
            (name,
             None if bone.parent is None else bone.parent.name,
             list(bone.tail)))

    to_save = pd.DataFrame(bone_table,
                           columns=bone_table_header)

    print(to_save)

    #: Save the data
    print('Saving file {}'.format(path))

    to_save.to_csv(path_or_buf=path,
                   na_rep='None',
                   index=False,
                   index_label=False)

    #: Switch back to OBJECT mode
    bpy.ops.object.mode_set(mode='OBJECT')


#######################################################################
#: Clear all items in the file
clear_world()

#: Create a Rig
#: initialize a bone table
bone_table = [('base', None, (0, 1, 0)),
              ('mid', 'base', (0, 1, 0)),
              ('tip', 'mid', (0, 1.5, 0))]

armature_gen = create_rig(name='chain', origin=(
    0., 0., 0.), bone_table=bone_table)

#: Pose table
pose_table = [('base', (90, 0, 0)),
              ('mid', (90, 0, 0)),
              ('tip', (90, 0, 0))]

#: Set the poses
pose_rig(armature_gen, pose_table)

save_rig(armature_gen)
######################################################################

# #: Creating the visuals for the pendulum
# bpy.ops.mesh.primitive_cube_add(radius=2, location=(0, 0, 5))
