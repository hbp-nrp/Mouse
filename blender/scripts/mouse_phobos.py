import bpy
import os
import sys
import imp
import numpy as np

dir = os.path.join(os.path.dirname(bpy.data.filepath), 'scripts')
sys.path.append(
    '/home/tatarama/Stata-PhD/Projects/BioRobAnimals/mouse/modeling/'
    'blender/')
if not dir in sys.path:
    sys.path.append(dir)
    print(sys.path)

import blender_read_meshes as read_meshes
from biorob_blender_modeling import armature
imp.reload(read_meshes)
imp.reload(armature)


def create_link(mesh_name, link_name):
    """ Create link """
    for obj in bpy.data.objects:
        obj.select = False
    body_mesh = bpy.data.objects[mesh_name]
    bpy.context.scene.objects.active = body_mesh
    body_mesh.select = True
    # Visual
    bpy.ops.phobos.set_phobostype(phobostype="visual")
    bpy.ops.phobos.define_geometry(geomType="mesh")
    # Create links
    print("Creating {}".format(link_name))
    bpy.ops.phobos.create_links(
        location="selected objects",
        size=1,
        parent_link=True,
        parent_objects=True,
        nameformat=link_name,
        linkname=link_name+"_"
    )
    body_mesh.select = False
    return


def create_joint(
        link_name, maxeffort=1e6, maxvelocity=1e3,
        min_angle=-np.pi, max_angle=np.pi, spring=0, damping=1e2,
        joint_type="revolute"
):
    """ Create joint """
    for obj in bpy.data.objects:
        obj.select = False
    link = bpy.data.objects[link_name]
    link.select = True
    bpy.ops.phobos.define_joint_constraints(
        passive=False,
        useRadian=True,
        joint_type=joint_type,
        lower=min_angle,
        upper=max_angle,
        maxeffort=maxeffort,
        maxvelocity=maxvelocity,
        spring=spring,
        damping=damping
    )
    link.select = False
    return


#: Clear the world
print("Clearing world!")
armature.clear_world()


#: read meshes
mesh_reader = read_meshes.ReadMeshes(mesh_path='./meshes/stl',
                                     mesh_type='stl')
mesh_reader.read()

#: Load armature
origin = (0, -12.5825, 5.27158)

bone_table = armature.read_rig('conf/mouse_skeleton_rig.csv')

armature_gen = armature.create_rig(name='double', origin=origin,
                                   bone_table=bone_table, scale=1,
                                   bone_connect=False)

# Parenting - Mesh to mesh
for (bone_name, parent_name, location) in bone_table:
    if not (parent_name == "None"):
        print(parent_name)
        try:
            parent = bpy.data.objects[parent_name]
            child = bpy.data.objects[bone_name]
        except KeyError:
            continue
        # child.parent = parent
        bpy.ops.object.select_all(action='DESELECT')
        parent.select = True
        child.select = True
        bpy.context.scene.objects.active = parent
        bpy.ops.object.parent_set(
            type='OBJECT',
            xmirror=False,
            keep_transform=False
        )
        bpy.ops.object.select_all(action='DESELECT')

        create_link(bone_name, bone_name+"_link")
        create_joint(bone_name+"_link")
        armature = bpy.data.objects[bone_name+"_link"].data
        bpy.ops.object.mode_set(mode='EDIT')
        for j, bone in enumerate(armature.edit_bones):
            bone.tail = (0, 0, 0.05)
        bpy.ops.object.mode_set(mode='OBJECT')
    else:
        pass
