"""Script to import original mesh files from blender mouse model."""

import os
import shutil
import sys

import bpy


class ReadMeshes(object):
    """Read meshes and load into blender.

    """

    def __init__(self, mesh_path, mesh_type):
        super(ReadMeshes, self).__init__()
        self.mesh_path = mesh_path
        self.mesh_type = mesh_type

    def read(self):
        """Read and load the meshes"""
        #: Mesh path
        self.mesh_path = bpy.path.abspath('//' + self.mesh_path)
        #: Check if the path is valid
        if os.path.isdir(self.mesh_path):
            print('Valid file path')
        else:
            print('Invalid file path')
            raise AttributeError()
            return
        #: Import the files to blender
        files = os.listdir(self.mesh_path)
        for file in files:
            if os.path.splitext(file)[-1] == '.'+self.mesh_type.lower():
                #: Import stl
                bpy.ops.import_mesh.stl(
                    filepath=os.path.join(self.mesh_path, file))
                print('Imported mesh -> {}'.format(file))
