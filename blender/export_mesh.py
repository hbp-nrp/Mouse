"""Script to export original mesh files from blender mouse model."""

import os
import shutil
import sys

import bpy

#: Read all the meshes in the scene
meshes = bpy.data.meshes

#: File path to save the meshes. Relative folder
mesh_dir = './'

#: Mesh folder name. Default is meshes
mesh_folder_name = 'meshes'

#: Mesh path
mesh_path = bpy.path.abspath('//' + mesh_dir + mesh_folder_name)

#: Check if a directory already exists and ask user to delete and
#: continue or exit the exporting.
if os.path.isdir(mesh_path):
    check = input('Folder already exists! Do you want to delete'
                  ' and continue? y or n :')

    if check[-1].lower() == 'y':
        print('Deleting the existing folder and continuing...')
        shutil.rmtree(mesh_path)
    else:
        print('Aborting exporting meshes...')
else:
    print('Creating new directory for meshes...')
    os.mkdir(mesh_path)
    os.mkdir(os.path.join(mesh_path, 'stl'))
    os.mkdir(os.path.join(mesh_path, 'dae'))

#: EXPORTING MESHES
for name, obj in bpy.data.objects.items():
    print('Exporting mesh : {}'.format(name))
    bpy.context.scene.objects.active = obj
    obj.select = True
    if obj.type == 'MESH':
        #: Export to stl
        bpy.ops.export_mesh.stl(filepath=(os.path.join(mesh_path, 'stl',
                                                       name+'.stl')),
                                use_selection=True)
        #: Export to dae
        bpy.ops.wm.collada_export(filepath=(os.path.join(mesh_path, 'dae',
                                                         name+'.dae')),
                                  selected=True)
