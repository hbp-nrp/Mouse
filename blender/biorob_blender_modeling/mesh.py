"""Script to import original mesh files from blender mouse model."""
import os
import shutil
import bpy


def write(mesh_dir='./', mesh_name_table=None):
    "Write meshes from the blender scene,"

    #: Select an object to switch to object mode
    _obj = bpy.data.objects[0]
    bpy.context.scene.objects.active = _obj
    _obj.select = True

    #: Switch to object mode
    bpy.ops.object.mode_set(mode='OBJECT')

    #: Unselect all objects
    bpy.ops.object.select_all(action='DESELECT')

    #: Read all the meshes in the scene
    meshes = bpy.data.meshes

    #: Mesh folder name. Default is meshes
    mesh_folder_name = 'meshes'

    mesh_path = os.path.join(mesh_dir, mesh_folder_name)

    #: Mesh path
    mesh_path = bpy.path.abspath('//' + mesh_path)

    #: Check if a directory already exists and ask user to delete and
    #: continue or exit the exporting.
    if os.path.isdir(mesh_path):
        check = input('Folder already exists! Do you want to delete'
                      ' and continue? y or n :')

        if check[-1].lower() == 'y':
            print('Deleting the existing folder and continuing...')
            shutil.rmtree(mesh_path)
        else:
            print('Aborting exporting meshes...')
            return

    print('Creating new directory for meshes...')
    os.mkdir(mesh_path)
    os.mkdir(os.path.join(mesh_path, 'stl'))
    os.mkdir(os.path.join(mesh_path, 'dae'))

    #: EXPORTING MESHES
    for name, obj in bpy.data.objects.items():
        #: Rename meshes from the table if available.
        if mesh_name_table:
            if mesh_name_table.get(name):
                print(name)
                name = mesh_name_table[name]
        print('Exporting mesh : {}'.format(name))
        bpy.context.scene.objects.active = obj
        obj.select = True
        if obj.type == 'MESH':
            #: Export to stl
            bpy.ops.export_mesh.stl(filepath=(os.path.join(mesh_path, 'stl',
                                                           name+'.stl')),
                                    use_selection=True)
            #: Export to dae
            bpy.ops.wm.collada_export(filepath=(os.path.join(mesh_path, 'dae',
                                                             name+'.dae')),
                                      selected=True)
        #: Unselect all objects
        bpy.ops.object.select_all(action='DESELECT')


def read(mesh_path='', mesh_type='.stl', scale=1):
    """Read and load the meshes"""

    #: Select an object to switch to object mode
    _obj = bpy.data.objects[0]
    bpy.context.scene.objects.active = _obj
    _obj.select = True

    #: Switch to object mode
    bpy.ops.object.mode_set(mode='OBJECT')

    #: Unselect all objects
    bpy.ops.object.select_all(action='DESELECT')

    #: Read all the meshes in the scene
    meshes = bpy.data.meshes

    #: Mesh path
    mesh_path = bpy.path.abspath('//' + mesh_path)
    #: Check if the path is valid
    if os.path.isdir(mesh_path):
        print('Valid file path')
    else:
        print('Invalid file path')
        raise AttributeError()
    #: Import the files to blender
    for mesh in os.listdir(mesh_path):
        if os.path.splitext(mesh)[-1] == '.'+mesh_type.lower():
            #: Import stl
            bpy.ops.import_mesh.stl(
                filepath=os.path.join(mesh_path, mesh),
                global_scale=scale)
            print('Imported mesh -> {}'.format(mesh))
